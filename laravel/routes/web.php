<?php
use App\Services;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('users.index');
// });


/*--frontend --*/
Route::get('/', 'IndexController@getPortfolioDatawithLimit');
Route::get('/portfolio', 'PortfolioController@getPortfolioList');
Route::post('/enquiry', 'IndexController@postEnquiry');
Route::get('/contact-us','IndexController@contactus');
Route::get('/product-service',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.product-services')->with($data);
 });
Route::get('/our-team',function () {
	$data['services']=Services::all();
	$data['pagename']='ourteam';
     return view('users.ourteam')->with($data);
 });
Route::get('/web-development',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.web-development')->with($data);
 });
Route::get('/e-commerce',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.e-commerce')->with($data);
 });
Route::get('/mobile-application',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.mobile-application')->with($data);
 });
Route::get('/search-engine-optimization',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.search-engine-optimization')->with($data);
 });
Route::get('/website-design-company',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.website-design-company')->with($data);
 });
Route::get('/smo',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.smo')->with($data);
 });
Route::get('/erp',function () {
	$data['services']=Services::all();
	$data['pagename']='product-service';
     return view('users.erp')->with($data);
 });
Route::post('/addcontactus','IndexController@addcontactus');
Auth::routes();


Route::get("/sendmail",'MyController@send');

Route::get('/admin/dashboard', 'PortfolioController@index');

Route::get('/career', 'CareerController@index');
Route::get('/career-apply/{id}', 'CareerController@careerApply');
Route::post("/jobenquiry/add",'CareerController@addJobEnquiry');
Route::post("/contactus",'ContactUsController@addcontctus');












Route::group(['middleware' => 'auth'], function(){

Route::get('/home', 'PortfolioController@index')->name('home');
	/*--backend portfolio--*/
	Route::get('/admin/portfolio', 'PortfolioController@index');
	Route::get('/admin/portfolio/add','PortfolioController@add');
	Route::post('/admin/portfolio/store','PortfolioController@store');
	Route::post('/admin/portfolio/delete','PortfolioController@destroy');
	Route::get('/admin/portfolio/edit/{id}', 'PortfolioController@edit');
	Route::post('/admin/portfolio/edit/{id}', 'PortfolioController@update');
	Route::post('/admin/portfolio/removeimage/{id}', 'PortfolioController@removeimage');

	/*--backend portfolio type--*/
	Route::get('/admin/portfoliotype', 'PortfolioTypeController@index');
	Route::get('/admin/portfoliotype/add', 'PortfolioTypeController@add');
	Route::post('/admin/portfoliotype/add', 'PortfolioTypeController@store');
	Route::get('/admin/portfoliotype/edit/{id}', 'PortfolioTypeController@edit');
	Route::post('/admin/portfoliotype/edit/{id}', 'PortfolioTypeController@update');
	Route::post('/admin/portfoliotype/delete','PortfolioTypeController@destroy');

	/*--backend services--*/
	Route::get('/admin/services', 'ServicesController@index');
	Route::get('/admin/services/add', 'ServicesController@add');
	Route::post('/admin/services/add', 'ServicesController@store');
	Route::get('/admin/services/edit/{id}', 'ServicesController@edit');
	Route::post('/admin/services/edit/{id}', 'ServicesController@update');
	Route::post('/admin/services/delete','ServicesController@destroy');

	/*--backend enquires--*/
	Route::get('/admin/enquiries', 'EnquiryController@index');
	Route::post('/admin/enquiries/delete','EnquiryController@destroy');
	Route::post('/enquiries/messageview','EnquiryController@ajaxMessageView');
	Route::post('/enquiries/commentview','EnquiryController@ajaxCommentView');
	Route::get('/admin/enquiries/addcomment/{id}','EnquiryController@addComment');
	Route::post('/admin/enquiries/addcomment/{id}','EnquiryController@storeComment');

	/*--backend category--*/
	Route::get('/admin/category', 'CategoriesController@index');
	Route::get('/admin/category/add', 'CategoriesController@add');
	Route::post('/admin/category/add', 'CategoriesController@store');
	Route::get('/admin/category/edit/{id}', 'CategoriesController@edit');
	Route::post('/admin/category/edit/{id}', 'CategoriesController@update');
	Route::post('/admin/category/delete','CategoriesController@destroy');

	/*--backend city--*/
	Route::get('/admin/city', 'CitiesController@index');
	Route::get('/admin/city/add', 'CitiesController@add');
	Route::post('/admin/city/add', 'CitiesController@store');
	Route::get('/admin/city/edit/{id}', 'CitiesController@edit');
	Route::post('/admin/city/edit/{id}', 'CitiesController@update');
	Route::post('/admin/city/delete','CitiesController@destroy');

	/*--backend job Descriptions--*/
	Route::get('/admin/job', 'JobdescriptionController@index');
	Route::get('/admin/job/add', 'JobdescriptionController@add');
	Route::post('/admin/job/add', 'JobdescriptionController@store');
	Route::get('/admin/job/edit/{id}', 'JobdescriptionController@edit');
	Route::post('/admin/job/edit/{id}', 'JobdescriptionController@update');
	Route::post('/admin/job/delete','JobdescriptionController@destroy');

	/*--backend job Descriptions--*/
	Route::get('/admin/jobenquiry', 'JobEnquiryController@index');
	Route::post('/admin/jobenquiry/delete', 'JobEnquiryController@destroy');
	Route::post('/jobenquiries/commentview','JobEnquiryController@ajaxCommentView');
});

Route::get('/logout',function () {
     return view('\login');
 });


