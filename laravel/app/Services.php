<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $primaryKey = 'service_id';
    protected $table = 'services';
    protected $fillable = [
        'service_name'
    ];
}
