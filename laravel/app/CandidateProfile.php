<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateProfile extends Model
{
    protected $primaryKey = 'jobprofile_id';
    protected $table = 'candidate_profile';
    protected $fillable = [
        'jobloc_id','canddidate_profile'
    ];
}