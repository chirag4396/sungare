<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $primaryKey = 'cu_id';
    protected $fillable = [
        'cu_name','cu_email','cu_subject','cu_message'
    ];	
    protected $table="contact_us";
    public $timestamps=false;
}
