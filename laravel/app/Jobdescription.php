<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobdescription extends Model
{
	protected $primaryKey = 'jobdesc_id';
    protected $table = 'jobdescriptions';
    protected $fillable = [
        'job_description','skill_content','candidate_profile','city_id','cat_id'
    ];
}
