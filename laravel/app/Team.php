<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team';

    protected $primaryKey = 't_id';
    
     protected $fillable = [
        't_fname','t_lname','t_designation_id','t_photo'
    ];
}
