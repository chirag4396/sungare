<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class JobEnquiry extends Model
{
    protected $primaryKey = 'je_id';
    protected $table = 'job_enquiry';
    protected $fillable = [
        'je_name','cat_id','je_email','je_phone','je_city_id','je_state_id','je_resume','je_current_city'
    ];

    public $timestamps = false;
}