<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductServices extends Model
{
    protected $table = 'product_services';

    protected $primaryKey = 'ps_id';
    
     protected $fillable = [
        'ps_title','ps_image'
    ];
}
