<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{

    protected $table = 'portfolio';

    protected $primaryKey = 'iPortfolioId';
    
     protected $fillable = [
        'vPortfolioName', 'portfoliotype_id','tPortfolioImage','tPortfolioUrl',
    ];

    const CREATED_AT = 'dtCreatedDateTime';
    const UPDATED_AT = 'dtUpdatedDateTime';
}
