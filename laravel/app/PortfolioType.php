<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioType extends Model
{
    protected $primaryKey = 'portfoliotype_id';
    protected $table = 'portfolio_types';
    protected $fillable = [
        'portfoliotype_name'
    ];
}
