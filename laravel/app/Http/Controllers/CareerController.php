<?php

namespace App\Http\Controllers;

use App\Career;
use App\Categories;
use App\Services;
use App\Cities;
use App\JobdescCategory;
use Illuminate\Http\Request;
use App\JobLocations;
use App\Jobdescription;
use App\JobSkills;
use App\CandidateProfile;
use App\JobEnquiry;
use App\State;
use Redirect;
class CareerController extends Controller
{
    use MyController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_category=Categories::orderBy('cat_id', 'desc')->get();
        $services =Services::orderBy('service_id', 'desc')->get();
        foreach($all_category as $key=>$value)
        {
            $job_desc=Jobdescription::where(['cat_id'=>$value->cat_id])->first();
            $locations=explode("|", $job_desc['city_id']);
            $location_name=array();
            foreach ($locations as $k => $v) {
                $loc=Cities::find($v);
                $location_name[$k]=$loc['city_name'];
            }
            $all_category[$key]->locations=implode("|", $location_name);
        }
        $data=array('all_category'=>$all_category,'services'=>$services,'pagename'=>'career');
        return view('users.career')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    function careerApply($id,Request $request)
    {
        $career=Categories::find($id);
        $job_descriptions= Jobdescription::where(['cat_id'=>$id])->first();
        //$job_descriptions=JobdescCategory::where('cat_id', '=', $id)->first();
         $locations=explode("|", $job_descriptions['city_id']);

         $loc=array();
            $location_name=array();
            foreach ($locations as $k => $v) {
                $loc[$k]=Cities::find($v);
                $location_name[$k]=$loc[$k]['city_name'];
            }
        $career_locations=array();
        $career_locations=$location_name;
        $career_locations['job_descriptions']=$job_descriptions['job_description'];
        $career_locations['skill_content']=$job_descriptions['skill_content'];
        $career_locations['candidate_profile']=$job_descriptions['candidate_profile'];
        $career_locations['city_name']=implode("|", $location_name);
       // print_r($career_locations);exit;
        $states=State::all();
        $all_location=Cities::orderBy('city_id', 'desc')->get();
        $services =Services::orderBy('service_id', 'desc')->get();
        $data=array('services'=>$services,'career'=>$career,'career_locations'=>$career_locations,'job_descriptions'=>$job_descriptions,'states'=>$states,'cities'=>$loc,'pagename'=>'career');
        return view('users.career_apply')->with($data);   
    }

    public function addJobEnquiry(Request $request)
    {
        $messages = array('je_name.required'=>'Name field is required','je_email.required'=>'Email field is required','je_current_city.required'=>'Current city field is required','je_state_id.required'=>'State field is required','je_resume.required'=>'Please Upload your resume','je_city_id.required'=>'Please select city for job apply','je_email.email'=>'Email is not valid.','je_resume.mimes'=>"For resume only .doc or .docx or .pdf file are allowed.");
        $rules = array(
            'je_name' => 'required',
            'je_email' => 'required|email|max:255',
            'je_phone' => 'required',
            'je_city_id' => 'required',
            'je_state_id' => 'required',
            'je_current_city' => 'required',
            'je_resume' => 'required|mimes:doc,docx,pdf',
        );
        $validator = \Validator::make($request->all(), $rules,$messages);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $je_id=JobEnquiry::create($request->all())->je_id;
         if($request->je_resume)
        {
            if ($request->hasFile('je_resume')) {
                
                $path=$this->fileUpload('job_enquiry',$request->file('je_resume')->getClientOriginalName());
                $split=explode("|", $path);
                $img_path=chop($split[0],$split[1]);
                $res =  $request->file('je_resume')->move($img_path, $split[1]);
                $request->tPortfolioImage = $split[1];
                $update_arr['je_resume'] = $request->tPortfolioImage;

            }
            JobEnquiry::find($je_id)->fill($update_arr)->save();
        }
        if($je_id>0)
        {
            $city=Cities::find($request->input('je_city_id'));
            $cityname=$city['city_name'];
            $state=State::find($request->input('je_state_id'));
            $statename=$state['state_name'];
            $category=Categories::find($request->input('cat_id'));
            $category_name=$category['cat_name'];
            $attachment='';
            if(file_exists($split[0]))
            {
                $attachment=$this->get_file_path('job_enquiry',$split[1]);
            }
            $email_data=array('client_name'=>$request->input('je_name'),'client_email'=>$request->input('je_email'),'je_phone'=>$request->input('je_phone'),'je_current_city'=>$request->input('je_current_city'),'je_city'=>$cityname,'je_state'=>$statename,'category_name'=>$category_name);
             //$this->send_mail($email_data,'users.jobenquiry_template','Job Enquiry Details',$attachment);
            $request->session()->flash("message",'Thank you for submitting your resume.');
        }
        return redirect('/career-apply/'.$request->input('cat_id'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function show(Career $career)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function edit(Career $career)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Career $career)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function destroy(Career $career)
    {
        //
    }
}
