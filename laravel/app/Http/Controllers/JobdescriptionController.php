<?php

namespace App\Http\Controllers;

use App\Jobdescription;
use Illuminate\Http\Request;
use App\Categories;
use App\Cities;
use Redirect;
class JobdescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_job=Jobdescription::leftJoin('categories', function($join) {
          $join->on('jobdescriptions.cat_id', '=', 'categories.cat_id');
        })->orderBy('jobdescriptions.jobdesc_id','desc')->get();
        foreach ($all_job as $key => $value) {
            $city_id=explode("|",$value->city_id);
            $city_name=array();
            foreach ($city_id as $k => $v) {
                $city=Cities::find($v);
                $city_name[$k]=$city['city_name'];
            }
            $all_job[$key]->city_name=implode(",", $city_name);
        }
        $data['pagename']='job';
        $data['all_job']=$all_job;
        return view('backend.jobdesc')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $categories=Categories::all();
        $locations=Cities::all();
        $data=array('pagename'=>'job','mode'=>'Add','categories'=>$categories,'locations'=>$locations);
        return view('backend.add_update_jobdesc')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages=array('cat_id.required'=>'The job category field is required','city_id.required'=>"The job location field is required");
         $rules = array(
            'job_description' => 'required',
            'skill_content' => 'required',
            'candidate_profile' => 'required',
            'city_id'=>'required',
            'cat_id'=>'required'
        );
        $validator = \Validator::make($request->all(), $rules,$messages);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $recordExist=Jobdescription::where([
                    ['cat_id', '=', $request->input('cat_id')]])->get();
        //echo count($recordExist);exit;
        if(count($recordExist)>0)
        {
            $error['errors']="Job Description already exists for selected category, choose another one..";
            return Redirect::back()->withErrors($error);
        }
        //print_r(implode("|", $request->input('city_id')) );exit;
        $city_id=implode("|", $request->input('city_id'));
        $request['city_id']=$city_id;
      //  print_r($city_id);exit;
        $jobdesc_id=Jobdescription::create($request->all())->jobdesc_id;
         $data=array();
       
        if($jobdesc_id>0)
        {
            $request->session()->flash('message', 'Record added successfully');
        }
        return redirect('/admin/job');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobdescription  $jobdescription
     * @return \Illuminate\Http\Response
     */
    public function show(Jobdescription $jobdescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobdescription  $jobdescription
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobdescription $jobdescription,$id)
    {
        $job=Jobdescription::find($id);
        $locations_selected=array();
        $locations_selected=explode("|", $job['city_id']);
        if(is_null($job))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/job');
        }
        $categories=Categories::all();
        $locations=Cities::all();
        $data=array('job'=>$job,'mode'=>'Update','categories'=>$categories,'locations'=>$locations,'pagename'=>'job','locations_selected'=>$locations_selected);
        return view('backend.add_update_jobdesc')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobdescription  $jobdescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $messages=array('cat_id.required'=>'The job category field is required','city_id.required'=>"The job location field is required");
         $rules = array(
            'job_description' => 'required',
            'skill_content' => 'required',
            'candidate_profile' => 'required',
            'city_id'=>'required',
            'cat_id'=>'required'
        );
        $validator = \Validator::make($request->all(), $rules,$messages);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $recordExist=Jobdescription::where([
                    ['cat_id', '=', $request->input('cat_id')],['jobdesc_id','!=',$id]])->get();
        if(count($recordExist)>0)
        {
            $error['errors']="Job Description already exists for selected category and loaction choose another one..";
            return Redirect::back()->withErrors($error);
        }
        if($request->input('city_id'))
        {
            $request['city_id']=implode("|", $request->input('city_id'));
        }
        $result=Jobdescription::find($id)->fill($request->all())->save();
       
        if($result>0)
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        return redirect('/admin/job');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobdescription  $jobdescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $result=Jobdescription::whereIn('jobdesc_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/job');
    }
}
