<?php

namespace App\Http\Controllers;
use App\Enquiry;
use Illuminate\Http\Request;
use App\Services;

class EnquiryController extends Controller
{
    public function postEnquiry(Request $request)
    {
    	$enquiry_data = $request->all();
    	unset($enquiry_data['_token']);
    	
       	$enquiry_result = Enquiry::create($enquiry_data);
       	print_r($enquiry_result);exit;
       	if($enquiry_result){
        	echo "success";exit;
       	}
       	else
       	{
       		echo "failure";exit;
       	}
    }

    public function index()
    {
        $all_enquiries=Enquiry::orderBy('enquiry_id', 'desc')->get();
       
        foreach ($all_enquiries as $key => $value) {
            $all_enquiries[$key]->service_name=Services::find($value->service_id)->service_name;
        }
        
        $data=array('pagename'=>'enquiry','all_enquiries'=>$all_enquiries);
        return view('backend.enquiries')->with($data);
    }
    public function ajaxCommentView(Request $request)
    {
        $enquiry_details=Enquiry::find($request->input('id'));
        $comment=($enquiry_details->enquiry_comments!='')?$enquiry_details->enquiry_comments:'No Comment';
         $message=($enquiry_details->client_message!='')?$enquiry_details->client_message:'No Message';
        $enquiryStr='';
        $datetime=date('jS M Y h:i:s a',strtotime($enquiry_details->created_at));
        $enquiryStr.='<table  class="table"><thead><tr><th width="60%">Posted Date Time:</th><td width="40%">'.$datetime.'</td></tr><tr><th width="40%">Client Name</th><th width="60%">Client Message</th></tr></thead>';
        $enquiryStr.='<tbody><tr><td>'.$enquiry_details->client_name.'</td><td>'.$message.'</td></tr><tr><td colspan="2"></td></tr><tr><td width="40%" style="font-weight: bold;">Comments:</td><td>'.$comment.'</td></tr></tbody></table>';
       return response()->json($enquiryStr);
    }
    function addComment(Enquiry $enquiry,$id)
    {
        $enquiry=Enquiry::find($id);
        $data=array('pagename'=>'enquiry','enquiry'=>$enquiry);
        return view('backend.enquiry_comment')->with($data);
    }
    function storeComment(Request $request,$id)
    {
        if($request->input('enquiry_comments')!='')
        {
            $enquiry=Enquiry::find($id)->fill($request->all())->save();
            if(!is_null($enquiry))
            {
                $request->session()->flash('message','Comment added successfully');
            }
        }
        return redirect('/admin/enquiries');
    }
    public function destroy(Request $request)
    {
        $result=Enquiry::whereIn('enquiry_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/enquiries');
    }


}
