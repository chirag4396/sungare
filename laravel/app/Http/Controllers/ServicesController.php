<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;
use Redirect; 

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_services=Services::orderBy('service_id', 'desc')->get();
        $data=array('pagename'=>'services','all_services'=>$all_services);
        return view('backend.services')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data=array('pagename'=>'services','mode'=>'Add');
        return view('backend.add_update_services')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'service_name' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $service_id=Services::create($request->all())->service_id;
        if($service_id>0)
        {
            $request->session()->flash('message', 'Record added successfully');
        }
        return redirect('/admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Services $services)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $services,$id)
    {
        $services=$services->find($id);
        if(is_null($services))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/services');
        }
        $data=array("services"=>$services,"mode"=>'Update',"pagename"=>'services');
        return view('backend.add_update_services')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$service_id)
    {

        $result=Services::find($service_id)->fill($request->all())->save();
        if($result)
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        else
        {
            $request->session()->flash('message',"Due to some technical error we can't update record.");  
        }
        return redirect('/admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
         $result=Services::whereIn('service_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/portfoliotype');
    }
}
