<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use View;
use App\Common;
use App\Portfolio;
use App\Enquiry;
use App\Contactus;
use Redirect;

class IndexController extends Controller
{
    use MyController;
     function getPortfolioDatawithLimit()
    {
    	$tablename="portfolio";
    	$limit=6;
		$portfolio = Common::fetchDataWithoutCondition($tablename,$limit);
		foreach ($portfolio as $key => $value) {
            
            $conditions=[['portfoliotype_id', '=', $value->portfoliotype_id]];
    		$portfoliotype=Common::fetchDataWithConditions('portfolio_type','single',$conditions);
             $portfolio[$key]->tPortfolioImage=$this->get_file_path('portfolio',$value->tPortfolioImage);
            if(isset($portfoliotype))
            {
                $portfolio[$key]->ePortfolioType=$portfoliotype->portfoliotype_name;
            }
            else
            {
                $portfolio[$key]->ePortfolioType='';
            }
        }
        $data['pagename']='home';
        $services=Common::fetchDataWithoutCondition('services');
        return view('users/index',['portfolio'=>$portfolio,'services'=>$services,'pagename'=>'home']); 
    }
    public function postEnquiry(Request $request)
    {
        
        $enquiryData=$request->all();
         $messsages = array(
            'service_id.required'=>'Please choose service',
            'client_name.required'=>'Please enter your name',
            'client_email.required'=>'Please enter your email',
            'client_phone_num.required'=>'Please enter your phone number',
        );

        $rules = array(
            'service_id'=>'required',
            'client_name' => 'required',
            'client_email' => 'required',
            'client_phone_num' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules,$messsages);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else
        {
            $services=Common::fetchDatawithConditions('services','single',array('service_id'=>$enquiryData['service_id']));
            $servicename=$services->service_name;
            $enquiryData['service_name']=$servicename;
            $enquiryId = Enquiry::create($request->all())->enquiry_id;
            if($enquiryId>0)
            {
                $this->send_mail($enquiryData,'users.enquirytemplate','Enquiry Details');
               echo "Your enquiry has been submitted, Thank you...";exit;
            }
            else
            {
                echo "Sorry, Due to some technical reason your enquiry not submitted";exit;
            }
        }
       
    }

    function contactus(Request $request)
    {
         $services=Common::fetchDataWithoutCondition('services');
         $data=array('services'=>$services,'pagename'=>'contactus');
        return view('users.contact_us')->with($data);
    }

    function addcontactus(Request $request)
    {
         $messsages = array(
            'cu_name.required'=>'Please enter your name',
            'cu_email.required'=>'Please enter your email',
            'cu_email.email'=>'Please enter valid email',
            'cu_subject.required'=>'Please enter subject',
            'cu_message.required'=>'Please enter your message',
        );

        $rules = array(
            'cu_name'=>'required',
            'cu_email' => 'required|email',
            'cu_subject' => 'required',
            'cu_message' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules,$messsages);
        if ($validator->fails())
        {
             return Redirect::back()->withErrors($validator);
        }
        $cu_id=Contactus::create($request->all())->cu_id;
        $contcatdata=array('client_name'=>$request->input('cu_name'),'client_email'=>$request->input('cu_email'),'client_message'=>$request->input('cu_message'));
        if($cu_id>0)
        {
            $this->send_mail($contcatdata,'users.contact_us_template',$request->input('cu_subject'));
            $request->session()->flash("message",'Thank you for contacting us..');
        }
        return redirect('/contact-us');
    }
}
