<?php

namespace App\Http\Controllers;

use App\Cities;
use Illuminate\Http\Request;
use Redirect;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_city=Cities::orderBy('city_id', 'desc')->get();
        $data['pagename']='city';
        $data['all_city']=$all_city;
        return view('backend.cities')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data['pagename']='city';
        $data['mode']='Add';
        return view('backend.add_update_cities')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'city_name' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $cat_id=Cities::create($request->all())->cat_id;
        if($cat_id>0)
        {
            $request->session()->flash('message', 'Record added successfully');
        }
        return redirect('/admin/city');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cities  $cities
     * @return \Illuminate\Http\Response
     */
    public function show(cities $cities)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cities  $cities
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $cities=Cities::find($id);
        if(is_null($cities))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/city');
        }
        $data=array("city"=>$cities,"mode"=>'Update',"pagename"=>'city');
        return view('backend.add_update_cities')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cities  $cities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $city_id)
    {
        $rules = array(
            'city_name' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $result=Cities::find($city_id)->fill($request->all())->save();
        if($result)
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        else
        {
            $request->session()->flash('message',"Due to some technical error we can't update record.");  
        }
        return redirect('/admin/city');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cities  $cities
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $result=Cities::whereIn('city_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/city');
    }
}
