<?php

namespace App\Http\Controllers;

use App\PortfolioType;
use Illuminate\Http\Request;
use Redirect; 

class PortfolioTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_portfoliotype=PortfolioType::orderBy('portfoliotype_id', 'desc')->get();
        $data['pagename']='portfoliotype';
        $data['all_portfoliotype']=$all_portfoliotype;
        return view('backend.portfoliotype')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data['pagename']='portfoliotype';
        $data['mode']='Add';
        return view('backend.add_update_portfoliotype')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'portfoliotype_name' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $portfoliotype_id=PortfolioType::create($request->all())->portfoliotype_id;
        if($portfoliotype_id>0)
        {
            $request->session()->flash('message', 'Record added successfully');
        }
        return redirect('/admin/portfoliotype');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PortfolioType  $portfolioType
     * @return \Illuminate\Http\Response
     */
    public function show(PortfolioType $portfolioType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PortfolioType  $portfolioType
     * @return \Illuminate\Http\Response
     */
    public function edit(PortfolioType $portfolioType,$id,Request $request)
    {
        $portfoliotype=$portfolioType->find($id);
        if(is_null($portfoliotype))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/portfoliotype');
        }
        $data=array("portfoliotype"=>$portfoliotype,"mode"=>'Update',"pagename"=>'portfoliotype');
        return view('backend.add_update_portfoliotype')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PortfolioType  $portfolioType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $portfoliotype_id)
    {
        $result=PortfolioType::find($portfoliotype_id)->fill($request->all())->save();
        if($result)
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        else
        {
            $request->session()->flash('message',"Due to some technical error we can't update record.");  
        }
        return redirect('/admin/portfoliotype');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PortfolioType  $portfolioType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $result=PortfolioType::whereIn('portfoliotype_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/portfoliotype');
    }
}
