<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use Redirect;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_category=Categories::orderBy('cat_id', 'desc')->get();
        $data['pagename']='category';
        $data['all_category']=$all_category;
        return view('backend.category')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data['pagename']='category';
        $data['mode']='Add';
        return view('backend.add_update_categories')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'cat_name' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $cat_id=Categories::create($request->all())->cat_id;
        if($cat_id>0)
        {
            $request->session()->flash('message', 'Record added successfully');
        }
        return redirect('/admin/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $categories=Categories::find($id);
        if(is_null($categories))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/categories');
        }
        $data=array("category"=>$categories,"mode"=>'Update',"pagename"=>'category');
        return view('backend.add_update_categories')->with($data);
    }
    public function update(Request $request, $cat_id)
    {
        $result=Categories::find($cat_id)->fill($request->all())->save();
        if($result)
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        else
        {
            $request->session()->flash('message',"Due to some technical error we can't update record.");  
        }
        return redirect('/admin/category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $result=Categories::whereIn('cat_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/category');
    }
}
