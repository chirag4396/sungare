<?php

namespace App\Http\Controllers;
use PHPMailer\PHPMailer;
use Illuminate\Http\Request;
use App\Common;
use Mail;
use File;
//Load Composer's autoloader

trait MyController
{

   public function send_mail($data,$emailtemplate,$subject,$attachment=NULL)
   {
      $attachment=$attachment;
        Mail::send($emailtemplate, $data, function($message) use ($data,$subject,$attachment){
             $message->to('shyam.sungare@gmail.com', 'Shyam Magar')->subject
             ($subject);
              $message->to('sungaretechnologies@gmail.com', 'Sungare Technologies')->subject
             ($subject);
              $message->to('kinjal.sungare@gmail.com', 'Kinjal Prajapati')->subject
             ($subject);
             $message->from($data['client_email'],$data['client_name']);
             if($attachment!=NULL)
             {
                $message->attach($attachment);
             }

          });
        return;
      
   }

   public function get_file_path($folder,$filename,$parent_folder='uploads')
   {
        $base_path=$_SERVER['DOCUMENT_ROOT'];
        $base_url=$_SERVER['HTTP_HOST'];
        $url=$base_path.'/'.$parent_folder.'/'.$folder.'/'.$filename;
        if(file_exists($url))
        {
            $file_path='http://'.$base_url.'/'.$parent_folder.'/'.$folder.'/'.$filename;
        }
        else
        {
            $file_path='http://'.$base_url.'/'.$parent_folder.'/no_image.png';
        }
        return $file_path;
   }

   public function fileUpload($folder,$image)
   {
		$base_path=$_SERVER['DOCUMENT_ROOT'].'/uploads/'.$folder;
        $result = File::makeDirectory($base_path, 0777, true, true);
        $name = date('YmdHis').'_'.$image;
		$file_name = preg_replace("/[^a-zA-Z0-9.]/", "_", $name);
        $filepath = $base_path.'/'.$file_name;
        return $filepath .'|'. $file_name;
   }

   public function deleteImage($image_path)
   {
		if(File::exists($image_path)) {
		    File::delete($image_path);
		    return 1;
		}
		return 0;
   }

}
