<?php

namespace App\Http\Controllers;
use App\TeamDesignation;
use Illuminate\Http\Request;
use App\Team;
use App\TeamDesignation;

class TeamController extends Controller
{
	use MyController;

	function index(Request $request)
    {
        
        $team_members=Team::orderBy('t_id', 'desc')->get();
        foreach ($team_members as $key => $value) {
            $team_members[$key]['t_photo']=$this->get_file_path('team',$value->t_photo);
            $team_designation=TeamDesignation::find($value->t_designation_id);
            $team_members[$key]['t_designation_id']=$team_designation['td_title'];
        }
        $data['pagename']='team';
        $data['team_members']=$team_members;
        return view('backend.team')->with($data);
    }

    function add()
    {
        $data['pagename']='team';
        $data['mode']='Add';
        $data['team_designation']=TeamDesignation::all();
        return view('backend.add_update_team')->with($data);
    }

    function store(Request $request)
    {
        $messages = array(
            't_name.required'=>'Team member name is required',
            't_name.min'=>'Team member name have been min 2 to maximum 255 characters',
            't_name.max'=>'Team member name have been min 2 to maximum 255 characters',
            't_photo.required'=>'Team member image is required',
            't_photo.mimes'=>'Only jpeg.png or jpg file is allowed',
            
            't_designation_id.required'=>'Team member designation is required',
        );
        $rules = array(
            't_name' => 'required|min:2|max:255',
            't_photo' => 'required|image|mimes:jpg,png,jpeg',
            't_designation_id' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules,$messages);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $t_id = Team::create($request->all())->t_id;
        
        if($request->t_photo)
        {
            if ($request->hasFile('t_photo')) {
                
                $path=$this->fileUpload('team',$request->file('t_photo')->getClientOriginalName());
                $split=explode("|", $path);
                $img_path=chop($split[0],$split[1]);
                $res =  $request->file('t_photo')->move($img_path, $split[1]);
                $request->t_photo = $split[1];
                $update_arr['t_photo'] = $request->t_photo;

            }
            Team::find($t_id)->fill($update_arr)->save();
        }
        $request->session()->flash('message', 'Record added successfully');
        return redirect('/admin/team');
    }
    public function removeImage(Team $team)
    {
        $image_path=$_SERVER['DOCUMENT_ROOT'].'/uploads/team/'.$team->t_photo;
        return $this->deleteImage($image_path);
    }
    public function destroy(Request $request)
    {
        $id=array();
            $id=$request->input('id');
            foreach ($id as $key => $value) {
                $teamData=Team::find($value);
                $this->removeImage($teamData);
            }
        $result=Team::whereIn('t_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/team');
    }
    public function edit(Team $team,$id)
    {
        $team=$team->find($id);
        $team->t_photo=$this->get_file_path('team',$team->t_photo);
        if(is_null($team))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/team');
        }
        $team_designation=TeamDesignation::all();
        $data=array("portfolio"=>$portfolio,"mode"=>'Update',"pagename"=>'portfolio','portfoliotypes'=>$portfoliotypes);
        return view('backend.add_update_portfolio')->with($data);
    }


}