<?php

namespace App\Http\Controllers;
use App\Enquiry;
use App\JobEnquiry;
use Illuminate\Http\Request;
use App\Categories;
use App\Cities;
use App\State;

class JobEnquiryController extends Controller
{
    use MyController;
    public function index()
    {
        
        $all_enquiries=JobEnquiry::orderBy('je_id', 'desc')->get();
       
        foreach ($all_enquiries as $key => $value) {
            $cat=Categories::find($value->cat_id);
           $all_enquiries[$key]->category=$cat['cat_name'];
           $all_enquiries[$key]->je_resume=$this->get_file_path('job_enquiry',$value->je_resume);
        }
        
        $data=array('pagename'=>'jobenquiry','all_enquiries'=>$all_enquiries);
        return view('backend.jobenquiries')->with($data);
    }
    public function ajaxCommentView(Request $request)
    {
        $enquiry_details=JobEnquiry::find($request->input('id'));
        $category=Categories::find($enquiry_details['cat_id']);
        $state=State::find($enquiry_details['je_state_id']);
        $city=Cities::find($enquiry_details['je_city_id']);
        $enquiryStr='';
        $enquiryStr.='<table class="table"><tbody><tr><td width="40%">Candidate Name</td><td width="60%">'.$enquiry_details['je_name'].'</td></tr><tr><td width="40%">Candidate Email</td><td width="60%">'.$enquiry_details['je_email'].'</td></tr><tr><td width="40%">Candidate Phone</td><td width="60%">'.$enquiry_details['je_phone'].'</td></tr><tr><td width="40%">Enquiry For</td><td width="60%">'.$category->cat_name.','.$city->city_name.'</td></tr><tr><td width="40%">Current city</td><td width="60%">'.$enquiry_details['je_current_city'].'</td></tr><tr><td width="40%">State</td><td width="60%">'.$state->state_name.'</td></tr></tbody>';
        $enquiryStr.='</table>';
       return response()->json($enquiryStr);
    }
    function addComment(Enquiry $enquiry,$id)
    {
        $enquiry=Enquiry::find($id);
        $data=array('pagename'=>'enquiry','enquiry'=>$enquiry);
        return view('backend.enquiry_comment')->with($data);
    }
    function storeComment(Request $request,$id)
    {
        if($request->input('enquiry_comments')!='')
        {
            $enquiry=Enquiry::find($id)->fill($request->all())->save();
            if(!is_null($enquiry))
            {
                $request->session()->flash('message','Comment added successfully');
            }
        }
        return redirect('/admin/enquiries');
    }
    public function removeImage(JobEnquiry $j)
    {
        $image_path=$_SERVER['DOCUMENT_ROOT'].'/uploads/job_enquiry/'.$j->je_resume;
        return $this->deleteImage($image_path);
    }

    public function destroy(Request $request)
    {
        $id=array();
            $id=$request->input('id');
            foreach ($id as $key => $value) {
                $jobenquiries=JobEnquiry::find($value);
                $this->removeImage($jobenquiries);
            }
        $result=JobEnquiry::whereIn('je_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/jobenquiry');
    }
    


}
