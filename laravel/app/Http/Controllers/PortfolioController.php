<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use View;
use App\Common;
use App\Portfolio;
use App\PortfolioType;
use Redirect; 


class PortfolioController extends Controller
{
    use MyController;
     function getPortfolioList()
    {
    	$tablename="portfolio";
    	$portfolio=Portfolio::all();
    	foreach ($portfolio as $key => $value) {
            
            $conditions=[['portfoliotype_id', '=', $portfolio[$key]->portfoliotype_id]];
    		$portfoliotype=PortfolioType::find($value->portfoliotype_id);
             $portfolio[$key]->tPortfolioImage=$this->get_file_path('portfolio',$value->tPortfolioImage);
            if(isset($portfoliotype))
            {
                $portfolio[$key]->ePortfolioType=$portfoliotype->portfoliotype_name;
            }
            else
            {
                $portfolio[$key]->ePortfolioType='';
            }
            
    	}
        $services=Common::fetchDataWithoutCondition('services');
        return view('users/portfolio',['portfolio'=>$portfolio,'services'=>$services,'pagename'=>'portfolio','portfoliotypes'=>PortfolioType::all()]);
    	
    }

    function index(Request $request)
    {
        
        $all_portfolio=Portfolio::orderBy('iPortfolioId', 'desc')->get();
        foreach ($all_portfolio as $key => $value) {
            $all_portfolio[$key]['tPortfolioImage']=$this->get_file_path('portfolio',$value->tPortfolioImage);
            $portfoliotype=PortfolioType::find($value->portfoliotype_id);
            $all_portfolio[$key]['portfoliotype']=$portfoliotype['portfoliotype_name'];
        }
        $data['pagename']='portfolio';
        $data['all_portfolio']=$all_portfolio;
        return view('backend.portfolio')->with($data);
    }
    function add()
    {
        $data['pagename']='portfolio';
        $data['mode']='Add';
        $data['portfoliotypes']=PortfolioType::all();
        return view('backend.add_update_portfolio')->with($data);
    }
    function store(Request $request)
    {
        $messages = array(
            'vPortfolioName.required'=>'portfolio name is required',
            'vPortfolioName.min'=>'portfolio name have been min 2 to maximum 255 characters',
            'vPortfolioName.max'=>'portfolio name have been min 2 to maximum 255 characters',
            'tPortfolioImage.required'=>'portfolio image is required',
            'tPortfolioImage.mimes'=>'Only jpeg.png or jpg file is allowed',
            'tPortfolioUrl.required'=>'portfolio url is required',
            'tPortfolioUrl.regex'=>'portfolio url is not valid',
            'portfoliotype_id.required'=>'portfolio type is required',
        );
        $rules = array(
            'vPortfolioName' => 'required|min:2|max:255',
            'tPortfolioImage' => 'required|image|mimes:jpg,png,jpeg',
            'tPortfolioUrl' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'portfoliotype_id' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules,$messages);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        $iPortfolioId = Portfolio::create($request->all())->iPortfolioId;
        
        if($request->tPortfolioImage)
        {
            if ($request->hasFile('tPortfolioImage')) {
                
                $path=$this->fileUpload('portfolio',$request->file('tPortfolioImage')->getClientOriginalName());
                $split=explode("|", $path);
                $img_path=chop($split[0],$split[1]);
                $res =  $request->file('tPortfolioImage')->move($img_path, $split[1]);
                $request->tPortfolioImage = $split[1];
                $update_arr['tPortfolioImage'] = $request->tPortfolioImage;

            }
            Portfolio::find($iPortfolioId)->fill($update_arr)->save();
        }
        $request->session()->flash('message', 'Record added successfully');
        return redirect('/admin/portfolio');
    }
    public function destroy(Request $request)
    {
        $id=array();
            $id=$request->input('id');
            foreach ($id as $key => $value) {
                $portfolioData=Portfolio::find($value);
                $this->removeImage($portfolioData);
            }
        $result=Portfolio::whereIn('iPortfolioId', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/admin/portfolio');
    }
    public function removeImage(Portfolio $portfolio)
    {
        $image_path=$_SERVER['DOCUMENT_ROOT'].'/uploads/portfolio/'.$portfolio->tPortfolioImage;
        return $this->deleteImage($image_path);
    }
    public function edit(Portfolio $portfolio,$id)
    {
        $portfolio=$portfolio->find($id);
        $portfolio->tPortfolioImage=$this->get_file_path('portfolio',$portfolio->tPortfolioImage);
        if(is_null($portfolio))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/admin/portfolio');
        }
        $portfoliotypes=PortfolioType::all();
        $data=array("portfolio"=>$portfolio,"mode"=>'Update',"pagename"=>'portfolio','portfoliotypes'=>$portfoliotypes);
        return view('backend.add_update_portfolio')->with($data);
    }
     public function update(Request $request, $iPortfolioId)
    {
        $portfolioData=Portfolio::find($iPortfolioId);
        $result=Portfolio::find($iPortfolioId)->fill($request->all())->save();
        if($request->tPortfolioImage)
        {
            $this->removeImage($portfolioData);
            if ($request->hasFile('tPortfolioImage')) {
                
                $path=$this->fileUpload('portfolio',$request->file('tPortfolioImage')->getClientOriginalName());
                //echo $path;exit;
                $split=explode("|", $path);
                $img_path=chop($split[0],$split[1]);
                $res =  $request->file('tPortfolioImage')->move($img_path, $split[1]);
                $request->tPortfolioImage = $split[1];
                $update_arr['tPortfolioImage'] = $request->tPortfolioImage;
                 Portfolio::find($iPortfolioId)->fill($update_arr)->save();
                 $result=1;
            }
           
        }
        if(!is_null($result))
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        else
        {
            $request->session()->flash('message',"Due to some technical error we can't update record.");  
        }
        return redirect('/admin/portfolio');
    }
}
