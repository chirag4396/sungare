<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamDesignation extends Model
{
    protected $table = 'team_designation';

    protected $primaryKey = 'td_id';
    
     protected $fillable = [
        'td_title'
    ];
}
