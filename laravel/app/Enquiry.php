<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
	protected $primaryKey = 'enquiry_id';
    protected $fillable = [
        'client_name', 'client_email','client_phone_num','service_id','client_message','enquiry_comments'
    ];	

    
    // protected $table="enquiries";
}
