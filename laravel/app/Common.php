<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Common extends Model
{
    static function fetchDataWithoutCondition($tablename,$limit=0)
    {
    	$db=DB::table($tablename);
    	if($limit!=0)
    	{
    		$db=$db->limit($limit);
    	}
    	$portfolio=$db->get();
    	return $portfolio;
    }
    static public function fetchDatawithConditions($tablename,$fetch_type,$conditions)
    {
        $db=DB::table($tablename);
        $db=$db->where($conditions);
        if($fetch_type=='single')
        {
            $db=$db->first();
        }
        else
        {
            $db=$db->get();
        }
        return $db;
    }

    static public function saveData($tablename,$data)
    {
       
        $id=DB::table($tablename)->insertGetId($data);
        return $id;
    }
}
