@extends('users.layouts.master')

@push('header')
	
   	
@endpush
@section('content')
	

	<main role="main-home-wrapper" class="container  sungare-web-design no-p">
		<div class="col-md-12 job-head">  <h2>{{ $career->cat_name }} </h2>
		</div>
		<div class="clearfix"> </div>
		<div class="job-des"> 
			@if(count($career_locations)>0)
			<div class="col-md-12">
				
				<a class="tab">{{ $career_locations['city_name'] }}</a></li>
					
				
			</div>
			
			<div class="tab-content">
				
					<div class="col-md-7  b-r  tabcontains" id="">
						@if($career_locations['job_descriptions']!='')
							<h2 >Job Description </h2>
							
							<p class="decr">{{ strip_tags($career_locations['job_descriptions']) }}</p>

							<div class="clearfix"> </div>
						@endif
						
						@if($career_locations['skill_content'])
							<h2 >Skills Required</h2>
							<ul class="list-with-line">
								@foreach(explode("\n", $career_locations['skill_content']) as $k1=>$v1)
								<li>{{ strip_tags($v1) }}</li>
								@endforeach
							</ul>
						@endif	

						<div class="clearfix"> </div>                     
						@if($career_locations['candidate_profile'])
							<h2 >Candidate Profile </h2>
							<ul class="list-with-line">
								@foreach(explode("\n", $career_locations['candidate_profile']) as $k2=>$v2)
								<li>{{ strip_tags($v2) }}</li>
								@endforeach
								
								
							</ul>
						@endif	
					
					</div>
				
			</div>
			@else
				<div class="col-md-7">
					<h2>No Vacancy avaliable</h2>
				</div>
			@endif
			@if($career_locations['city_name']=='')
			<div class="col-md-7">
					<h2>No Vacancy avaliable</h2>
				</div>
			@endif
			<div class="col-md-5"><div class="main-resume">
				@if(Session::has('message'))
					<div class="col-12">
						<div class="alert alert-success alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  
		                  {{ Session::get('message') }}
		                </div>
					</div>
				@endif
				@if (count($errors)) 
				      <div class="alert alert-danger alert-dismissible error-box" id="error-box">
							<ul>
					       		@foreach($errors->all() as $error) 
					        
					            	<li style="margin:10px;">{{ $error }}</li>
					          
								@endforeach 
							</ul>
					 	</div>    
					@endif
				<div class="form1">
					<h2 style="color: #01285a; text-align: center;" >Apply Now </h2>
					<form action="/jobenquiry/add" method="post" enctype="multipart/form-data" name="job_enquiry">
						<input name="_token" type="hidden" value="{{ csrf_token() }}" />
						<input type="hidden" name="cat_id" name="cat_id" value="{{ $career->cat_id }}">
						<div class="contact-details">
							<h2 style="border:none;font-size: 17px;text-align: center;">Contact Details </h2>

							<div class=" col-xs-12 mar-top">
								<input type="text" name="je_name" id="name" 
								class="form-control" placeholder="Your Name" min="2" max="25" pattern="[a-zA-Z]*" title="Only characters are allowed.">
							</div>

							<div class=" col-xs-12">
								<input type="email" name="je_email" id="email" 
								class="form-control" placeholder="Email Address">
							</div> 

							<div class=" col-xs-12">
								<input type="text" name="je_phone" id="phone_num" 
								class="form-control" placeholder="Mobile Number" pattern="[0-9]{10}" title="Phone number must be in 10 digits">
							</div>
							<div class="col-md-12 " >
								
								<select name="je_city_id" class="form-control text4" placeholder="Apply in City">
									<option value="">Apply in City</option>
								@foreach($cities as $key=>$value)
									<option value="{{ $cities[$key]['city_id'] }}">{{ $value['city_name'] }}</option>
								@endforeach 
							</select>
							</div>
							<div class="col-md-12 " >
								<input type="text" name="je_current_city" id="city" 
								class="form-control" placeholder=" Current City"> 
							</div>
							<div class="col-md-12"> 
								<select id="state" class="form-control" name="je_state_id" class="text4">
									<option value="">--Select State--</option>
									@foreach($states as $key=>$value)
										<option value="{{ $value->state_id }}">{{ $value->state_name }}</option>
									@endforeach
								</select>
							</div>
							<h2 style="border:none;font-size: 17px;text-align: center;" >Upload Resume </h2>
							<div class="col-md-6 mar-top ">
								<div class="choose">
									Choose File
									<input type="file" class="hide_file" name="je_resume" id="resume" onchange="myfile();" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
									
								</div>
								
								<!-- <input type="file" id="resume" name="resume"> -->
							</div>
							<div class="col-md-6 mar-top">
								<span id="filenametextbox"></span><br>
								<span style="color:red;">(.doc or .docx or pdf file only)</span>
								<span id="resumeError" class="red">
								</span>
							</div>
							{{-- <div class="col-md-12 mar-top  " >
								
								
								
							</div> --}}

							<div class="clearfix"> </div>



							<div class="col-md-12 text-center"> 
								<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary"><b style="color: #009eda;">Submit </b> </button>
							</div>
						</div>
					</form>
				</div>  
			</div>
		</div>
	</main>

	@push('footer')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css2">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		setTimeout(function() {
		$(".alert").hide()
	}, 5000);
		
	</script>
	@endpush
	 <script type="text/javascript">
	 		setTimeout(function() {
		$(".alert").hide()
	}, 5000);
	 window.onload = function(e){
		$('.headersec').addClass("o-t-main");
		
	}
		function myfile()
		{
			var name = document.getElementById('resume');
      	document.getElementById("filenametextbox").innerHTML=name.files.item(0).name;
      
		}
		
	</script>
	 </script>
@endsection