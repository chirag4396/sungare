@extends('users.layouts.master')

@section('content')
<section class="headersec" id="banner2">
		
		<div class="header">
			<div class="container">
				<div class="logo">
					<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
				</div> 

				<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
					<div class="widget-icon">
						<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
						
						<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
							></i></a>
							
					</div>
				</div>
				<div class="menu">

					<div class="one"><span>MENU</span></div>
					<div class="two"><a href="#" class="navicon"></a></div> 
					<div class="toggle effect-3"> 
						<ul class="toggle-menu">
							<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
							
							<li><a href="{{ URL::to('/product-service') }}" {{ ($pagename=='product-service')?"class=active":'' }}> Services</a></li>
							<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
							<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>						
							<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
							<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
						</ul>
					</div> 
				</div>   
					
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="clearfix"> </div>
		
		<div class="title">
			<h2>SEO</h2>
		</div>

	</section>

<main role="main-home-wrapper" class="container sungare-web-design no-p">
	<h2 class="block-title">We provide high-end <span class="color-red">Search Engine Optimization</span></h2>

	<div class="col-xs-12 no-p">
		<div class="col-lg-12 ">

			<p class="decr">We are here to offer you a qualified team of specialists who are ready to promote your website at any time. Every project is treated equally by a well-structured team of professionals including project manager, analyst, SEO expert, copywriter, PR manager, link manager, designer, coder, programmer, and a dedicated account manager. Our employees have several years of experience in the field of SEO and challenging expertise in the world of Online Marketing.we are  pride in creating memorable, exclusive and inspirational artistry for all clients. We promise to max out those achievement barriers to guarantee your brand is a reputable champion online. </p>
		</div>
		

		
		

		<div class="clearfix"></div>


		<div class="col-lg-12 other-ser ser">
			<div class="title">
				<h2>	How we can help your business grow</h2>
				<p>Done-for-you marketing services that get results</p>
			</div>


			<div class="col-lg-3 no-padding">
				<div class="col-lg-12 web1 font1 b-r b-t">
					
					<img src="{{ asset('img/s1.png') }}">
					<h5>Keyword Search</h5>
				</div>
				<div class="col-lg-12  web1 font1 b-r  ">

					<img src="{{ asset('img/s2.png') }}">
					<h5>Links Building</h5>
				</div>

			</div>
			
			<div class="col-lg-3 no-padding ">
				<div class="col-lg-12  web1 font1 b-r b-t ">

					<img src="{{ asset('img/s3.png') }}">
					<h5>Traffic Monitoring</h5>
				</div>
				
				<div class="col-lg-12  web1 font1 b-r  ">

					<img src="{{ asset('img/s4.png') }}">
					<h5>Ranking</h5>
				</div>
				
			</div>
			<div class="col-lg-3 no-padding ">
				<div class="col-lg-12  web1 font1 b-r b-t ">

					<img src="{{ asset('img/s5.png') }}">
					<h5>Social Network</h5>
				</div>
				<div class="col-lg-12  web1 font1 b-r  ">

					<img src="{{ asset('img/s6.png') }}">
					<h5>Sitemap Optimization</h5>
				</div>
			</div>
			<div class="col-lg-3 no-padding ">
				<div class="col-lg-12  web1 font1 b-r b-t ">

					<img src="{{ asset('img/s7.png') }}">
					<h5>Content</h5>
				</div>

				<div class="col-lg-12  web1 font1 b-r  ">

					<img src="{{ asset('img/s8.png') }}">
					<h5>Feedback</h5>
				</div>
			</div>
		</div>
		
		
	</main>
	<a href="{{ URL::to('/portfolio') }}"><div class="ban1">
		<img style="cursor:pointer;" class="img3" src="{{ asset('img/ban1.png') }}">

	</div></a>



	<div class="other-ser">

		<div class="container">


			<div class="title">
				<h2>OTHER SERVICES</h2>
			</div>
			
			
			<a href="{{ asset('smo') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a3.png') }}">
				<h4>SMO</h4>
			</div></a>
			<a href="{{ asset('mobile-application') }}"><div class="col-lg-2 col-xs-6 b-r">
				
				<img src="{{ asset('img/a4.png') }}">
				<h4>Android Application </h4>
			</div></a>
			<a href="{{ asset('erp') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a5.png') }}">
				<h4>ERP Development</h4>
			</div></a>
			
			<a href="{{ asset('website-design-company') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a7.png') }}">
				<h4>Web Design</h4>
			</div></a>
			<a href="{{ asset('web-development') }}">
				<div class="col-lg-2 col-xs-6  "><img src="{{ asset('img/a1.png') }}"><h4>Web Development</h4></div></a>
			<a href="{{ asset('e-commerce') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a6.png') }}">
				<h4>E-commerce</h4>
			</div></a>
		</div>
	</div>
	<div class="text-center en">
    <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary open-switcher show-switcher"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
    <h5>+91 7020914154</h5>
    <h5>sales@sungare.com</h5>
</div>
@endsection
@push('footer')
	<script type="text/javascript">
		
	</script>
	@endpush