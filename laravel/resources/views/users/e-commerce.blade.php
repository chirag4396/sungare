@extends('users.layouts.master')

@section('content')
<section class="headersec" id="banner2">
		
		<div class="header">
			<div class="container">
				<div class="logo">
					<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
				</div> 

				<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
					<div class="widget-icon">
						<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
						
						<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
							></i></a>
							
					</div>
				</div>
				<div class="menu">

					<div class="one"><span>MENU</span></div>
					<div class="two"><a href="#" class="navicon"></a></div> 
					<div class="toggle effect-3"> 
						<ul class="toggle-menu">
							<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
							
							<li><a href="{{ URL::to('/product-service') }}" {{ ($pagename=='product-service')?"class=active":'' }}> Services</a></li>
							<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
							<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>						
							<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
							<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
						</ul>
					</div> 
				</div>   
					
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="clearfix"> </div>
		
		<div class="title">
			<h2>E-commerce</h2>
		</div>

	</section>
<main role="main-home-wrapper" class="container sungare-web-design no-p">
	<h2 class="block-title">Build your site on the most flexible, most powerful  </br><span class="color-red">E-commerce platform</span></h2>
	<div class="col-xs-12">

		<div class="col-lg-12 no-p ">

			<div class="col-lg-12 other-ser no-p ser" style="margin-bottom: 0px;">
				

				
				<div class="col-lg-12 no-p ">
					<p class="decr">Our ERP system works seamlessly with SAP Business One to manage business processes like Production, Supply Chain Planning and Quality Control.he next generation ERP Software, helps you capitalize by unifying traditional manufacturing capabilities into a new purpose-built solutionSAP Business One with OptiProERP targets the six critical operational areas central to any manufacturing companyutomates and streamlines business processes, so your business runs faster and
					more profitabl</p>
				</div>
				
				

				<div class="clearfix"></div>
				<div class="col-lg-12 no-p ">
					<img class="e-c img3"  src="{{ asset('img/e1.png') }}">	
				</div>

				<div class="clearfix"></div>
				<div class="col-lg-12 no-p">
					<div class="col-lg-6 no-p b-r">
						<ul class="list-with-line">
							<li>Connect online, shops, and warehouses with , and warehouses with live, and warehouses with live live stock data</li>
							<li>Send sales metrics to your f, and warehouses with live, and warehouses with live, and warehouses with liveinance tools for greater insights</li>
							<li>Run targetted marketing c, and warehouses with live, and warehouses with live, and warehouses with liveampaigns with customer profiling</li>
							
						</ul>
					</div>
					<div class="col-lg-6 no-p">
						<ul class="list-with-line">
							<li>Connect online, shops, and warehouses with , and warehouses with live, and warehouses with live live stock data</li>
							<li>Send sales metrics to your f, and warehouses with live, and warehouses with live, and warehouses with liveinance tools for greater insights</li>
							<li>Run targetted marketing c, and warehouses with live, and warehouses with live, and warehouses with liveampaigns with customer profiling</li>
							
						</ul>
					</div>

					<div class="clearfix"></div>

					
				</div>
				
			</div>
		</div>
		<div class="clearfix"></div>

		
	</div>
	
</div>





<div class="clearfix"></div>


</main>
<a href="{{ URL::to('/portfolio') }}"><div class="ban1">
		<img style="cursor:pointer;" class="img3" src="{{ asset('img/ban1.png') }}">

	</div></a>





<div class="other-ser">

	<div class="container">


		<div class="title">
			<h2>OTHER SERVICES</h2>
		</div>
		
		<a href="{{ asset('search-engine-optimization') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a2.png') }}">
				<h4>SEO</h4>
			</div></a>
			
			<a href="{{ asset('smo') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a3.png') }}">
				<h4>SMO</h4>
			</div></a>
			<a href="{{ asset('mobile-application') }}"><div class="col-lg-2 col-xs-6 b-r">
				
				<img src="{{ asset('img/a4.png') }}">
				<h4>Android Application </h4>
			</div></a>
			<a href="{{ asset('erp') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a5.png') }}">
				<h4>ERP Development</h4>
			</div></a>
			
			<a href="{{ asset('website-design-company') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a7.png') }}">
				<h4>Web Design</h4>
			</div></a>
			
			<a href="{{ asset('web-development') }}">
				<div class="col-lg-2 col-xs-6  "><img src="{{ asset('img/a1.png') }}"><h4>Web Development</h4></div></a>
		
	</div>
</div>
<div class="text-center en">
    <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary open-switcher show-switcher"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
    <h5>+91 7020914154</h5>
    <h5>sales@sungare.com</h5>
</div>
@endsection
@push('footer')
	
	@endpush