<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="switcher-box">
    <a class="open-switcher show-switcher">
        <p>Enquire Now</p>
    </a>

    <div class="enquiry">
        <div class="title" style="text-align: center;padding-bottom: 10px;">
          <h2>Enquire Now</h2>
    </div>
    <div class="loader" style="padding: 10px;text-align: center;display: none;">
        <img src="images/loader.gif" width="95%" height="50px"  />
    </div>
    
    <form id="form_enquiry" method="post">
        <div class="alert alert-danger" style="display:none"></div>
        <div class="col-xs-12 err_div" style="display: none;">
            <span class="form-control err_msg" style="color:red;"></span>
        </div>
        <input type="hidden" name="enquiry_comments">
        <div class=" col-xs-12">
            <select class="form-control" name="service_id" id="service_id">
                <option value="">Select Service</option>
                @foreach($services as $allservices)
                <option value="{{ $allservices->service_id }}">{{ $allservices->service_name }}</option>
                @endforeach
            </select>        
        </div>
        <div class=" col-xs-12">
            <input type="text" name="client_name" id="name" class="form-control" placeholder="Name" min="2" max="80" pattern="[A-Za-z]" title="Only characters are allowed."> 
             <span class="client_name_err" style="display: none;color:red;"></span>
        </div>
        <div class=" col-xs-12">
            <input type="text" name="client_email" id="email" class="form-control client_email" placeholder="E-mail"> 
            <span class="email_err" style="display: none;color:red;"></span>
        </div>
        <div class=" col-xs-12">
            <input type="text" name="client_phone_num" id="phone_num" class="form-control" placeholder="Mob Number"> 
             <span class="client_phone_err" style="display: none;color:red;"></span>
        </div>
        <div class=" col-xs-12">
            <textarea name="client_message" id="message" class="form-control" placeholder="Message"></textarea>
             
        </div>
        <div class="text-center">
            <input type="button" value="Submit" id="submit" class="btn btn-lg btn-sub btn-primary" onclick="post_enquiry()">
        </div>
    </form>
</div>
</div>
<script type="text/javascript">
    $('.client_email').keyup(function() {
  if ($('.client_email').val() != '') {
    var email=$('.client_email').val();
    if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
    {
       $('.email_err').show();
            $('.email_err').html('Please enter valid email address');
            return false; 
    }
    else
    {
        $('.email_err').hide();
        return true; 
    }
} });
     $('#name').keyup(function() {
  if ($('#name').val() != '') {
    var name=$('#name').val();
    if(name.match(/^[a-zA-Z]+[ ]+[a-zA-Z]*$/)==null && name.match(/^[a-zA-Z]*$/)==null)
    {
       $('.client_name_err').show();
            $('.client_name_err').html('Only characters are allowed.');
            return false; 
    }
    else
    {
        $('.client_name_err').hide();
        return true; 
    }
} });
    $('#phone_num').keyup(function() {
  if ($('#phone_num').val() != '') {
    var a = $('#phone_num').val();
     var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if(a.match(filter)==null)
    {
       $('.client_phone_err').show();
            $('.client_phone_err').html('Phone Number must be in digit and have atleast 10 digits.');
            return false;
    }
    else
    {
        $('.client_phone_err').hide();
    }
} });
    function post_enquiry() {
        var email=$('.client_email').val();
        var a = $('#phone_num').val();
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
       // alert(a.match(filter));return false;
        if (a.match(filter)==null && a!='') {
            

            $('.client_phone_err').show();
            $('.client_phone_err').html('Phone Number must be in digit and have atleast 10 digits');
            return false;
        }
        else
        {
            $('.client_phone_err').hide();
        }
        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null && email!='')
        {
            $('.email_err').show();
            $('.email_err').html('Please enter valid email address');
            return false;

        }
        else
        {
            $('.email_err').hide();
        }

        
        var fd = new FormData($('#form_enquiry')[0]);
        
        var url="{{URL::to('/enquiry')}}";
        $.ajax({
            type:'POST',
            url:url,
            data:fd,
            processData : false,
            contentType : false,
            success:function(data){
                //$('.loader').hide(); 
                if(data.errors)
                {
                    jQuery.each(data.errors, function(key, value){
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<p>'+value+'</p>');
                    });
                    setTimeout(function() {
                        $(".alert").hide()
                    }, 5000);
                }
                else
                {
                    $('.err_div').show();
                    $("form").trigger("reset");
                    $('.err_msg').html(data);
                    setTimeout(function() {
                        $(".err_div").hide();

                        if ($('.open-switcher').hasClass('hide-switcher')) {
                           $('.switcher-box').css({
                                'right': '-310px'
                            });
                            $('.open-switcher').removeClass('hide-switcher');
                            $('.open-switcher').addClass('show-switcher');
                        }   
                    }, 2500);
                            
                }
            },
        });
    }
</script>