@extends('users.layouts.master')

@push('header')
<link href="{{ asset('css/set2.css') }}" rel="stylesheet" type="text/css">
@endpush
@section('content')
<section id="banner2">

	<div class="header">
		<div class="container">
			<div class="logo">
				<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
			</div> 

			<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
				<div class="widget-icon">
					<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>

					<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
						></i></a>

					</div>
				</div>
				<div class="menu">

					<div class="one"><span>MENU</span></div>
					<div class="two"><a href="#" class="navicon"></a></div> 
					<div class="toggle effect-3"> 
						<ul class="toggle-menu">
							<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>

							<li><a href="{{ URL::to('/product-service') }}" {{ ($pagename=='product-service')?"class=active":'' }}> Services</a></li>
							<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
							<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>						
							<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
							<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
						</ul>
					</div> 
				</div>   

				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="title">
			<h2>Our Services</h2>
		</div>

	</section>
	<main role="main-home-wrapper" class="container">



		<div class="row">

			<div class="col-md-6 no-padding">

				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<article role="pge-title-content">

						<header>

							<h2><span>Sungare</span> A Brand new Agency.</h2>

						</header>

						<p>Technology changes rapidly and is expanding to new areas and devices encompassing our daily lives. By utilizing the best technology can offer in our services such as Web Designing, Web App Development, Mobile App Development and Internet Marketing, we help run businesses and applications with more stability and speed.</p>

					</article>

				</section>



				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-2.jpg') }}" alt="" class="img-responsive"> 

						<a href="{{ URL::to('website-design-company') }}"><figcaption>

							<h2><span> WEB</span> DESIGN</h2>




						</figcaption></a>

					</figure>

				</section>


				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-4.jpg') }}" alt="" class="img-responsive"> 

						<a href="{{ URL::to('e-commerce') }}"><figcaption>

							<h2><span> ECOMMERCE</span> Websites</h2>



						</figcaption></a>

					</figure>

				</section>


				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-5.jpg') }}" alt="" class="img-responsive"> 

						<a href="{{ URL::to('smo') }}"><figcaption>

							<h2><span> SMO</span> (SOCIAL MEDIA OPTIMIZATION)</h2>



						</figcaption></a>

					</figure>

				</section>


			</div>


			<div class="col-md-6 no-padding">

				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-6.jpg') }}" alt="" class="img-responsive">

						<a href="{{ URL::to('web-development') }}"><figcaption>

							<h2><span> WEB Application</span>  Development</h2>



						</figcaption></a>

					</figure>

				</section>


				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-3.jpg') }}" alt="" class="img-responsive">

						<a href="{{ URL::to('search-engine-optimization') }}"> <figcaption>

							<h2><span> SEO</span> (Search Engine Optimization)</h2>



						</figcaption></a>

					</figure>

				</section>



				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-7.jpg') }}" alt="" class="img-responsive"> 

						<a href="{{ URL::to('erp') }}"><figcaption>

							<h2><span> ERP</span> DEVELOPMENT</h2>



						</figcaption></a>

					</figure>

				</section>


				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 grid">

					<figure class="effect-oscar">

						<img src="{{ asset('images/image-8.jpg') }}" alt="" class="img-responsive"> 

						<a href="{{ URL::to('mobile-application') }}"><figcaption>

							<h2><span> ANDROID</span> APPLICATION</h2>



						</figcaption></a>

					</figure>

				</section>

			</div>

		</div>

	</main>
	<div class="text-center en">
		<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
		<h5>+91 7020914154</h5>
		<h5>sales@sungare.com</h5>

	</div>
	@endsection