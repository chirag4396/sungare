@extends('users.layouts.master')

@push('header')
	
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.fancybox.css') }}">
    <link href="{{ asset('css/set2.css') }}" rel="stylesheet" type="text/css">
@endpush
@section('content')
	<section id="banner2">
		<div class="header">
			<div class="container">
				<div class="logo">
					<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
				</div> 

				<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
					<div class="widget-icon">
						<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
						
						<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
							></i></a>
							
					</div>
					<div class="menu ">

						<div class="one"><span>MENU</span></div>
						<div class="two"><a href="#" class="navicon"></a></div> 
						<div class="toggle effect-3"> 
							<ul class="toggle-menu">
								<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
								
								<li><a href="{{ URL::to('/product-service') }}"> Services</a></li>
								<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
								<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>							
								<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
								<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
							</ul>
						</div> 
					</div>   
				</div>
			</div>
		</div>
		<div class="title">
			<h2>Career</h2>
		</div>
	</section>

	<main role="main-home-wrapper" class="container sungare-web-design no-p">

		<div class="hire">
			<h2 class="type-kilo--sans">
				Interested? We’re hiring.
			</h2>
			<div class="type-med--serif">
				We’re always searching for amazing people to join our team.
				<br> Take a look at our current openings.<br><br>

				<p style="color: #fff816;">"We recognise that our future success will depend on our ability to recruit,retain and develop the best people" For any type of career openings like web designers or PHP developer jobs refer following job openings .. or send your resume at <span style="color: #75b1ff;">hr@sungare.com</span></p>
			</div>
		</div>
		<div class="clearfix"> </div>
		<div class="col-xs-12 no-p">
			<section class="hire-main">
				<div class="">
					<div class="title">
						<h2>Current Openings</h2>
					</div>
				</div>
				<div class="col-md-12 grid">
					@foreach($all_category as $key=>$value)
						<div class="col-md-4">
							<figure class="effect-oscar2">
								@php 
								    $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

									$randomcolor = '#' . $rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
									
								@endphp
								<div class="job" style=" background:{{ $randomcolor }};">
									{{ $value['cat_name'] }}<br>

									<div class="place1" style="color: #fff;">{{ $value['locations'] }}</div>
								</div>
								
								<a href="{{ URL::to('/career-apply/'.$value["cat_id"]) }}">
									<figcaption>
										<h2><span> APPLY</span>NOW</h2>
									</figcaption> 
								</a>                      
						</div>
					@endforeach
				</div>
			</section>
		</div>
	</main>
	
@endsection