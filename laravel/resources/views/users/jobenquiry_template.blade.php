<h1>Job Enquiry Details</h1>
<h2>Job Enquiry For: {{ $category_name }}</h2>
<p>Candidate Name: {{ $client_name }}</p>
<p>Candidate Email: {{ $client_email }}</p>
<p>Candidate Mobile No : {{ $je_phone }}</p>
<p>Candidate Current City : {{ $je_current_city }}</p>
<p>Candidate State : {{ $je_state }}</p>
<p>Candidate Apply for city: {{ $je_city }}</p>