@extends('users.layouts.master')

@section('content')

<main role="" class="container sungare-web-design no-p">
	<h2 class="block-title">Geniuses behind our work
	</br>
	<span class="color-red"> meet our Key Team Members</span></h2>
</main>
<div id="sungares">
	<div class="sungare">
		<div class="memberImage img-responsive " style="background-image:url(images/shyam.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Shyam Magar<br><span>Sales Head</span></span></a>


	</div>
	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/2.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Amol Patil<br><span>Managing Director</span></span></a>


	</div>
	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/kiran.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span> Kiran Patil <br><span> Senior Program Manager</span></span></a>


	</div>
	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/ponny.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Ponny Chacko<br><span>UI Designer</span></span></a>


	</div>
	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/jasita.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Jasitha Udaykrishnan<br><span>Software Developer</span></span></a>


	</div>
	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/chirag.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Chirag Chhuchha<br><span>Software Developer</span></span></a>


	</div>


	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/avi.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Harshal Deshmukh<br><span> B. D. E</span></span></a>


	</div> 

	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/bh.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Bhakti Kadam <br><span>Software Tester </span></span></a>


	</div>

	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/ajit.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Ajit Patil<br><span>System Analyst</span></span></a>


	</div>

	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/ajay.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Ajay Patel<br><span>Jr. Software Developer</span></span></a>


	</div>
	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/ka.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Rohini Bonde <br><span>B. D. E </span></span></a>


	</div>

	<div class="sungare">
		<div class="memberImage" style="background-image:url(images/t1.jpg)"></div>
		<a href="#" class="memberName cboxElement" rel="team"><span>Ashish Kumar
			<br><span>B. D. E </span></span></a> 


		</div>

		<div class="sungare">
			<div class="memberImage" style="background-image:url(images/t2.jpg)"></div>
			<a href="#" class="memberName cboxElement" rel="team"><span>dashrath gavhane  <br><span>Web Designer </span></span></a>


		</div>

		<div class="sungare">
			<div class="memberImage" style="background-image:url(images/t3.jpg)"></div>
			<a href="#" class="memberName cboxElement" rel="team"><span>Megha  <br><span>Web Designer </span></span></a>




		</div>


		<div class="sungare">
			<div class="memberImage" style="background-image:url(images/t4.jpg)"></div>
			<a href="#" class="memberName cboxElement" rel="team"><span>Neha Jadhav
				<br><span>Web Designer </span></span></a>




			</div>


			<div class="sungare">
				<div class="memberImage" style="background-image:url(images/t5.jpg)"></div>
				<a href="#" class="memberName cboxElement" rel="team"><span>Suresh Tapkir 

					<br><span>Software Developer </span></span></a>




				</div>



				<div class="sungare">
					<div class="memberImage" style="background-image:url(images/t6.jpg)"></div>
					<a href="#" class="memberName cboxElement" rel="team"><span>Poonam Sherikar

						<br><span>
						HR </span></span></a>




					</div>


					<div class="sungare">
						<div class="memberImage" style="background-image:url(images/t7.jpg)"></div>
						<a href="#" class="memberName cboxElement" rel="team"><span>Dhaval Kotecha


							<br><span>
							Android developer </span></span></a>




						</div>


						<div class="sungare">
							<div class="memberImage" style="background-image:url(images/t8.jpg)"></div>
							<a href="#" class="memberName cboxElement" rel="team"><span>Simran Sayed


								<br><span>
								Content writer and Blogger </span></span></a>




							</div>


							<div class="sungare">
								<div class="memberImage" style="background-image:url(images/t9.jpg)"></div>
								<a href="#" class="memberName cboxElement" rel="team"><span>Payal tathe




									<br><span>
									Trainee </span></span></a>




								</div>


								<div style="display:none" class="111">1</div><div class="sungare textTile"><div class="ratio"></div><div class="ttWrap"><h3>Want to join our team?</h3>
									<p>We're always looking for talented designers, developers, project managers and anyone who's driven and has a passion for the IT industry..</p>
									<ul>
										<li><a href="/mca-internship-in-pune.php">Check our Career page</a></li>
										<li><a href="mailto:hr@sungare.com">Send us your CV</a></li>
									</ul></div></div><div style="clear:both"></div></div>




									<a href="Portfolio.html"><div class="ban1">
										<img style="cursor:pointer;" class="img3" src="img/ban1.png">

									</div></a>







									<div class="text-center en">
										<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
										<h5>+91 7020914154</h5>
										<h5>sales@sungare.com</h5>

									</div>









									<!-- footer start -->
									
									<!-- footer end -->
									<a href="#0" class="cd-top js-cd-top">Top</a>


									<!-- Style Switcher -->

									@push('footer')
									<script type="text/javascript">
										window.onload = function(e){
											$('.headersec').addClass("o-t-main");
										}
									</script>
									@endpush
									@endsection