@extends('users.layouts.master')

@push('header')
	
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/portfolio.css') }}">
@endpush
@section('content')
<section id="banner2">
    <div class="header">
        <div class="container">
            <div class="logo">
               <a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
           	</div> 

           	<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
              <div class="widget-icon">
                <a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
                 
                <a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin"></i></a>
                     
                </div>
            </div>
            <div class="menu">

                        <div class="one"><span>MENU</span></div>
                        <div class="two"><a href="#" class="navicon"></a></div> 
                        <div class="toggle effect-3"> 
                            <ul class="toggle-menu">
                                <li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
                                
                                <li><a href="{{ URL::to('/product-service') }}"> Services</a></li>
                                <li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
                                <li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>                         
                                <li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>                    
                                <li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
                            </ul>
                        </div> 
                    </div>   
      	<div class="clearfix"> </div>
     </div>
 	</div>  
 	<div class="clearfix"> </div>
	<div class="title">
 		<h2>Our Works</h2>
	</div>
</section>
<main role="main-home-wrapper" class="container sungare-web-design no-p">
	<h2 class="block-title"><span class="color-red">MAKE YOUR
    PRODUCT HAPPEN .. !!</span> <br> Get a beautifully crafted website with great user experience and conversion-focused design approach.</h2>
    <section class="filter-section" style="padding-top:0;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                 
                    <div class="filter-container isotopeFilters">
                        <ul class="list-inline filter">
                            <li class="active"><a href="#" data-filter="*">All </a></li>
                            @foreach($portfoliotypes as $value)
                            <li><a href="#" data-filter=".{{ $value->portfoliotype_name }}">{{ $value->portfoliotype_name }}
                            </a></li>
                            @endforeach
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="portfolio-section port-col">
        <div class="container">
            <div class="row">
                <div class="isotopeContainer">
                    @foreach($portfolio as $allPortfolio)
                    <div class="col-md-4 isotopeSelector {{$allPortfolio->ePortfolioType}}">
                        <a href="{{$allPortfolio->tPortfolioUrl}}" target="_blank"><article class="">
                            <figure>
                                <img src="{{$allPortfolio->tPortfolioImage}}" alt="">
                                <div class="overlay-background">
                                    <div class="inner"></div>
                                </div>
                                
                            </figure>
                            <div class="article-title"><a href="#">{{$allPortfolio->vPortfolioName}}</a></div>
                        </article></a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
                	
</main>
<div class="text-center en">
    <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary open-switcher show-switcher"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
    <h5>+91 7020914154</h5>
    <h5>sales@sungare.com</h5>
</div>

@push('footer')

<script src="js/jquery-1.11.3.min.js"></script> 
<script src="js/isotope.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/portfolio.js"></script> 


@endpush

@endsection
