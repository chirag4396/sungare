<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111417164-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-111417164-1');
	</script>
	<title>Website design company in Pune, Rajkot, Kottayam, India - Sungare</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<meta name="description" content="Sungare technologies Pvt.Ltd. is best website design company in pune. We are best website design company in Rajkot, Kottayam. Our services includes SEO ,digital marketing, E-commerce website. We design responsive and mobile friendly websites."/>
	
	<meta name="keywords" content="Best Website Design Company in Pune,  Rajkot, Kottayam,  Website design company in pune, Rajkot, Kottayam, MCA internship in pune, Software development company in pune, IT Company in pune, Web design company in pune, E-commerce website design company, seo company in Pune, Website design companies in pune, List of it company in pune, Digital marketing service in pune, Web design companies in pune, Website Design company in Rajkot" />

	<!-- Custom Theme files -->



	<link href="{{ asset('css/bootstrap.css') }}" type="text/css" rel="stylesheet" media="all">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }} ">
	<link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="all">   
	<link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
	<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> <!-- font-awesome icons --> 
	<link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
	
	<!-- //Custom Theme files --> 
	<!-- js -->

	<script src="{{ asset('js/jssor.slider-27.1.0.min.js') }}" type="text/javascript"></script>
	<!-- //js -->
	
	<meta name="google-site-verification" content="2Cp1dy6kzm-uINflTw9CWz9OHPqEt9ZLwN8Tu7pQIls" />
	
	<meta name="google-site-verification" content="jdn03HfT86o-rEO1gSjpmGZHM2F_FlhXEUOrxve3cJ0" />
	
	<!-- Start of Async Drift Code -->
	<script>
		!function() {
			var t;
			if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
				t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
				t.factory = function(e) {
					return function() {
						var n;
						return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
					};
				}, t.methods.forEach(function(e) {
					t[e] = t.factory(e);
				}), t.load = function(t) {
					var e, n, o, i;
					e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
					o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
					n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
				});
		}();
	drift.SNIPPET_VERSION = '0.3.1';
	drift.load('dzzm26n8ef9v');
</script>
<!-- End of Async Drift Code -->

@stack('header')
</head>
<body> 	
	<section class="headersec">
		
		<div class="header">
			<div class="container">
				<div class="logo">
					<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
				</div> 

				<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
					<div class="widget-icon">
						<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
						
						<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
							></i></a>
							
					</div>
				</div>
				<div class="menu">

					<div class="one"><span>MENU</span></div>
					<div class="two"><a href="#" class="navicon"></a></div> 
					<div class="toggle effect-3"> 
						<ul class="toggle-menu">
							<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
							
							<li><a href="{{ URL::to('/product-service') }}" {{ ($pagename=='product-service')?"class=active":'' }}> Services</a></li>
							<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
							<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>						
							<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
							<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
						</ul>
					</div> 
				</div>   
					
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="clearfix"> </div>
		
		<div class="title" style="display:none">

		</div>

	</section>
