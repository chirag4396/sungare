<!-- footer start -->
<footer>
	<!-- footer-top-area start -->
	<div class="footer-top-area">
		<div class="col-lg-12 m-b no-p">
			<!-- footer-widget start -->
			<div class="col-lg-6 no-p">
				<div class="col-lg-5 col-xs-12  col-md-3 col-sm-4">
					<div class="footer-widget">
						<img class="foo-l" src="{{ asset('images/logo7.png') }}" alt="" />
						
						
					</div>
				</div>
				<div class="col-lg-3 no-p col-xs-6 col-md-3 hidden-sm b-r ">
					<div class="footer-widget">

						<img class="img3"  src="{{ asset('img/call.png') }}" alt="" />
						<h4>CONNECT US</h4>

						<h5>+91 7020914154</h5>
						<h5>+91 9730816139</h5>

					</div>
				</div>
				<div class="col-lg-3 col-xs-6 col-md-3 hidden-sm no-p">
					<div class="footer-widget">
						<img class="img3" src="{{ asset('img/mail.png') }}" alt="" />
						<h4>MAIL US</h4>
						<h5>sales@sungare.com</h5>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 no-p  footer-widget">
				<h3> <i class="fa fa-map-marker"> </i> Our Locations</h3>
				<div class="col-lg-4 col-md-3 col-xs-6  col-sm-4 b-r no-p">
					<div class="footer-widget">
						
						<ul class="footer-menu">
							<li><span>Maharashtra</span></li>
							<img style="margin-top:0px" src="{{ asset('img/mm.png') }}">
							<li style="border:0;">Shrikrishna Building, </li>
							<li style="border:0;">Katepuram Chowk, </li>
							<li>Near. Pagare Hospital,</li>
							<li> Pimple Gurav - 411 061.</li>
							
						</ul>
					</div>
				</div>
				<div class="col-lg-4 no-p col-md-3  col-xs-6 col-sm-4 b-r">
					<div class="footer-widget">
						
						<ul class="footer-menu">
							<li><span>Gujarat</span></li>
							<img style="margin-top:0px" src="{{ asset('img/gg.png') }}">
							<li style="border:0;">Office no. 1 Raj Hill Apartment, </li>
							<li style="border:0;">Behind Ganga Hall, </li>
							<li >Amin Marg,</li>
							<li> Rajkot - 360 001</li>
							
						</ul>
					</div>
				</div>
				<div class="col-lg-4  no-p  col-md-3 col-xs-12 col-sm-4">
					<div class="footer-widget">
						<ul class="footer-menu">
							<li><span>Kerala</span></li>
							<img style="margin-top:0px" src="{{ asset('img/kk.png') }}">
							
							<li style="border:0;">State Highway 32,</li>
							<li>  Kidangoor,</li>
							<li>  Near Pala,</li>
							<li>  Kottayam - 686 572</li>
							
							
							
						</ul>
						
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
	<!-- footer-top-area end -->
	<!-- footer-bottom-area start -->
	<div class="clearfix"></div>
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-6 col-sm-6">
					<div class="copyright">
						<p>Copyright © <a href="#">Sungare</a>. All Rights Reserved</p>
					</div>
				</div>
				<div class="col-lg-7 col-md-6 col-sm-6 foo-menu">
					<ul >
						<li><a href="{{ URL::to('/') }}" class="active"> Home</a></li>
						
						<li><a href="{{ URL::to('/product-service') }}"> Services</a></li>
						<li><a href="{{ URL::to('/portfolio') }}">Our Works</a></li> 
						
						<li><a href="{{ URL::to('/career') }}">Career</a></li>				<li><a href="#">Blog</a></li>		
						<li><a href="{{ URL::to('/contact-us') }}"> Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- footer-bottom-area end -->
</footer>	
<!-- footer end -->
<a href="#0" class="cd-top js-cd-top">Top</a>

@include('users/enquiry')
<!-- Style Switcher -->




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	jssor_1_slider_init = function() {

		var jssor_1_SlideshowTransitions = [
		{$Duration:800,$Opacity:2}
		];

		var jssor_1_options = {
			$AutoPlay: 1,
			$SlideshowOptions: {
				$Class: $JssorSlideshowRunner$,
				$Transitions: jssor_1_SlideshowTransitions,
				$TransitionsOrder: 1
			},
			$ArrowNavigatorOptions: {
				$Class: $JssorArrowNavigator$
			},
			$BulletNavigatorOptions: {
				$Class: $JssorBulletNavigator$
			}
		};

		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		/*#region responsive code begin*/

		var MAX_WIDTH = 1349;

		function ScaleSlider() {
			var containerElement = jssor_1_slider.$Elmt.parentNode;
			var containerWidth = containerElement.clientWidth;

			if (containerWidth) {

				var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

				jssor_1_slider.$ScaleWidth(expectedWidth);
			}
			else {
				window.setTimeout(ScaleSlider, 30);
			}
		}

		ScaleSlider();

		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		/*#endregion responsive code end*/
	};
</script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {

		$('.open-switcher').click(function() {
			
			if ($(this).hasClass('show-switcher')) {
				$('.switcher-box').css({
					'right': 0
				});
				$('.open-switcher').removeClass('show-switcher');
				$('.open-switcher').addClass('hide-switcher');
			}           
			else if (jQuery(this).hasClass('hide-switcher')) {
				$('.switcher-box').css({
					'right': '-310px'
				});
				$('.open-switcher').removeClass('hide-switcher');
				$('.open-switcher').addClass('show-switcher');
			}
		});
	});
</script>

<script src="{{ asset('js/owl.carousel.min.js') }}"></script>

<script>
	$('.navicon').on('click', function (e) {
		e.preventDefault();
		$(this).toggleClass('navicon--active');
		$('.toggle').toggleClass('toggle--active');
	});
</script> 

<script>
	$(window).scroll(function(){
		if ($(window).scrollTop() > 50) {
			$('.header').addClass('sticky-menu'); 
		} else {
			$('.header').removeClass('sticky-menu');
		}
	}); // End Scroll Function


	$(window).on('resize', function(){
		autoHeight();
	}); // End Resize


	(function(){
		var topoffset = 0;
		$('#scroll').click(function() {
			if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top-topoffset
					}, 1000);
					return false;
          } // target.length
        } //location hostname
      }); //on click

		$.scrollUp({
			scrollDistance: 2000,
			scrollSpeed: 1200,
		});
	}())
</script>

<script type="text/javascript">jssor_1_slider_init();</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-54694170-1', 'auto');
	ga('send', 'pageview');

</script>
@stack('footer')

</body>
</html>