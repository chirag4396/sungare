@include('users.layouts.header')


<section id="about" class="about-wrapper" style="padding-top: 3em;">
	<div class="title text-center">
		<h2>Let’s Talk!</h2>
		<div class="container">

			<h3 class="block-title"> Whether you want to work with us or are interested in learning more about what we do, we’d love to hear from you.<span style="    color: #d80000;">There is no better time than now!!!</span> </h3>
		</div>

	</div><!-- end title -->
	<div class="contact_tab text-center">
		<ul id="myTab" class="container text-center nav nav-tabs" role="tablist">
			<li class="active"><a href="#tab1" role="tab" data-toggle="tab">Contact Details</a></li>
			<li><a href="#tab2" role="tab" data-toggle="tab">Location Maps</a></li>
		</ul>
		<div id="myTabContent" class="tab-content ">
			<div class="tab-pane fade in active" id="tab1">
				<div class="container">

					<div id="message"></div>



					<div class="col-md-6">
						@if(Session::has('message'))
					<div class="col-12">
						<div class="alert alert-success alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  
		                  {{ Session::get('message') }}
		                </div>
					</div>
				@endif
				@if (count($errors)) 
				      <div class="alert alert-danger alert-dismissible error-box" id="error-box">
							<ul>
					       		@foreach($errors->all() as $error) 
					        
					            	<li style="margin-left:10px;">{{ $error }}</li>
					          
								@endforeach 
							</ul>
					 	</div>    
					@endif

						<form id="contactform" action="{{ URL::to('/addcontactus') }}" name="contactform" method="post" data-scroll-reveal="enter from the bottom after 0.4s">
							<input name="_token" type="hidden" value="{{ csrf_token() }}" />
							<div class=" col-xs-12">
								<input type="text" name="cu_name" id="cu_name" class="form-control" placeholder="Name" min="2" max="80" title="Only characters are allowed." pattern="[a-zA-Z]*"> 
							</div>
							<div class=" col-xs-12">
								<input type="email" name="cu_email" id="cu_email" class="form-control" placeholder="Email Address"> 
							</div>
							<div class=" col-xs-12">
								<input type="text" name="cu_subject" id="subject" class="form-control" placeholder="Subject"> 
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<textarea class="form-control" name="cu_message" id="comments" rows="6" placeholder="Message"></textarea>
							</div>
							<div class="text-center">
								<input type="submit" value="SUBMIT" id="submit" class="btn btn-lg btn-contact btn-primary">
							</div>
						</form> <!-- End Form -->
					</div>




					<div class="col-md-6">

						<div class=" no-p  footer-widget">
							<h3> <i class="fa fa-map-marker"> </i> Our Locations</h3>
							<div class="col-lg-4 col-md-3 col-xs-12 col-xs-4 col-sm-4 b-r no-p">
								<div class="footer-widget">

									<ul class="footer-menu">
										<li><span>Maharashtra</span></li>
										<img style="margin-top:0px" src="img/mm.png">
										<li style="border:0;">Sri nagar Apt,</li>
										<li style="border:0;">shivaginagar</li>
										<li>Pune - 656431</li>

									</ul>
								</div>
							</div>
							<div class="col-lg-4 no-p col-md-3 col-xs-4 col-sm-4 b-r">
								<div class="footer-widget">

									<ul class="footer-menu">
										<li><span>Gujarat</span></li>
										<img style="margin-top:0px" src="img/gg.png">
										<li style="border:0;">Sri nagar Apt,</li>
										<li style="border:0;">shivaginagar</li>
										<li >Pune - 656431</li>

									</ul>
								</div>
							</div>
							<div class="col-lg-4  no-p col-xs-4 col-md-3 col-sm-4">
								<div class="footer-widget">
									<ul class="footer-menu">
										<li><span>Kerala</span></li>
										<img style="margin-top:0px" src="img/kk.png">
										<li style="border:0;">Sri nagar Apt,</li>
										<li style="border:0;">shivaginagar</li>
										<li>Pune - 656431</li>

									</ul>

								</div>
							</div>
						</div>

						<div class="clearfix"></div>


						<div class="sun-con">


							<div class="col-lg-6 no-p col-xs-6 col-md-3 hidden-sm  ">
								<div class="footer-widget">

									<img class="img3"  src="img/call.png" alt="" />
									<h4>CONNECT US</h4>

									<h5>+91 7020914154</h5>
									<h5>+91 9730816139</h5>

								</div>
							</div>

							<div class="col-lg-6 col-xs-6 col-md-3 hidden-sm no-p">
								<div class="footer-widget">
									<img class="img3" src="img/mail.png" alt="" />
									<h4>MAIL US</h4>
									<h5>sales@sungare.com</h5>
								</div>
							</div>

						</div>				
					</div>



					<div class="clearfix"></div>











				</div> <!-- End Container -->
			</div><!-- End Tab Pane -->

			<!-- /Google Map -->     
			<div class="tab-pane fade" id="tab2">
				<!-- Start Map Section -->
				<div class="map-area">      
					<div class="map">  
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3781.6956918024407!2d73.81418921442031!3d18.58775318736991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2b8b9cc5fff0d%3A0xda5290901928e8b9!2ssungare+technologies+pvt+ltd!5e0!3m2!1sen!2sin!4v1523342289128" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<!-- End Map Section -->
			</div>   
		</div><!-- /end my tab content -->  
	</div><!-- /contact_tab -->  


</section><!--/ Contact End -->  
<footer>
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-6 col-sm-6">
					<div class="copyright">
						<p>Copyright © <a href="#">Sungare</a>. All Rights Reserved</p>
					</div>
				</div>
				<div class="col-lg-7 col-md-6 col-sm-6 foo-menu">
					<ul >
						<li><a href="{{ URL::to('/') }}" class="active"> Home</a></li>
						
						<li><a href="{{ URL::to('/product-service') }}"> Services</a></li>
						<li><a href="{{ URL::to('/portfolio') }}">Our Works</a></li> 
						
						<li><a href="{{ URL::to('/career') }}">Career</a></li>				<li><a href="#">Blog</a></li>		
						<li><a href="{{ URL::to('/contact-us') }}"> Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	jssor_1_slider_init = function() {

		var jssor_1_SlideshowTransitions = [
		{$Duration:800,$Opacity:2}
		];

		var jssor_1_options = {
			$AutoPlay: 1,
			$SlideshowOptions: {
				$Class: $JssorSlideshowRunner$,
				$Transitions: jssor_1_SlideshowTransitions,
				$TransitionsOrder: 1
			},
			$ArrowNavigatorOptions: {
				$Class: $JssorArrowNavigator$
			},
			$BulletNavigatorOptions: {
				$Class: $JssorBulletNavigator$
			}
		};

		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		/*#region responsive code begin*/

		var MAX_WIDTH = 1349;

		function ScaleSlider() {
			var containerElement = jssor_1_slider.$Elmt.parentNode;
			var containerWidth = containerElement.clientWidth;

			if (containerWidth) {

				var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

				jssor_1_slider.$ScaleWidth(expectedWidth);
			}
			else {
				window.setTimeout(ScaleSlider, 30);
			}
		}

		ScaleSlider();

		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		/*#endregion responsive code end*/
	};
</script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {

		$('.open-switcher').click(function() {
			
			if ($(this).hasClass('show-switcher')) {
				$('.switcher-box').css({
					'right': 0
				});
				$('.open-switcher').removeClass('show-switcher');
				$('.open-switcher').addClass('hide-switcher');
			}           
			else if (jQuery(this).hasClass('hide-switcher')) {
				$('.switcher-box').css({
					'right': '-310px'
				});
				$('.open-switcher').removeClass('hide-switcher');
				$('.open-switcher').addClass('show-switcher');
			}
		});
	});
</script>

<script src="{{ asset('js/owl.carousel.min.js') }}"></script>

<script>
	$('.navicon').on('click', function (e) {
		e.preventDefault();
		$(this).toggleClass('navicon--active');
		$('.toggle').toggleClass('toggle--active');
	});
</script> 

<script>
	$(window).scroll(function(){
		if ($(window).scrollTop() > 50) {
			$('.header').addClass('sticky-menu'); 
		} else {
			$('.header').removeClass('sticky-menu');
		}
	}); // End Scroll Function


	$(window).on('resize', function(){
		autoHeight();
	}); // End Resize


	(function(){
		var topoffset = 0;
		$('#scroll').click(function() {
			if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top-topoffset
					}, 1000);
					return false;
          } // target.length
        } //location hostname
      }); //on click

		$.scrollUp({
			scrollDistance: 2000,
			scrollSpeed: 1200,
		});
	}())
</script>

<script type="text/javascript">jssor_1_slider_init();</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-54694170-1', 'auto');
	ga('send', 'pageview');
	setTimeout(function() {
		$(".alert").hide()
	}, 5000);
</script>