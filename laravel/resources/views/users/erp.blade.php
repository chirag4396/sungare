@extends('users.layouts.master')

@section('content')

<section class="headersec" id="banner2">
		
		<div class="header">
			<div class="container">
				<div class="logo">
					<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
				</div> 

				<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
					<div class="widget-icon">
						<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
						
						<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
							></i></a>
							
					</div>
				</div>
				<div class="menu">

					<div class="one"><span>MENU</span></div>
					<div class="two"><a href="#" class="navicon"></a></div> 
					<div class="toggle effect-3"> 
						<ul class="toggle-menu">
							<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
							
							<li><a href="{{ URL::to('/product-service') }}" {{ ($pagename=='product-service')?"class=active":'' }}> Services</a></li>
							<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
							<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>						
							<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
							<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
						</ul>
					</div> 
				</div>   
					
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="clearfix"> </div>
		
		<div class="title">
			<h2>ERP</h2>
		</div>

	</section>
<main role="main-home-wrapper" class="container sungare-web-design no-p">
	<h2 class="block-title">Achieve Manufacturing Excellence with our </br><span class="color-red">ERP Software</span></h2>
	<div class="col-xs-12 no-p">
		<div class="col-xs-12 no-p">

			<div class="col-lg-6 no-p">
				<div class="col-lg-12 no-p ser">
					
					<img class="img3" src="img/er.png">
					
				</div>
			</div>
			<div class="col-lg-6 ">

				<p class="decr">We have increased traffic, leads, and sales for well-known companies—including Dell, Mrs Fields Cookies, Hotels.com and H&R Block.

				Plus for hundreds of local smaller companies like dentists, plumbers, dermatologists, etc.When you optimize for either search environment, there are some direct and indirect benefits. The direct benefit is simply more people finding your profile and connecting with you and visiting your website. The indirect benefit is from an extra link poin profile and connecting with you and visiting your website. The indirect benefit is from an extra link pointing to your website from your profile.ly, never allow employees to use a personal e-mail address to create or manage your brand social media accounts. Alwayly. </p>
			</div>
			
			

			<div class="clearfix"></div>
			<div class="col-lg-12 no-p ">

				<div class="col-lg-12 no-p other-ser ser" style="margin-bottom: 0px;">
					<div class="title">
						<h2>	How we can help your business grow</h2>
						<p>Done-for-you marketing services that get results</p>
					</div>


					
					<div class="col-lg-6 ">
						<p class="decr">Our ERP system works seamlessly with SAP Business One to manage business processes like Production, Supply Chain Planning and Quality Control.he next generation ERP Software, helps you capitalize by unifying traditional manufacturing capabilities into a new purpose-built solutionSAP Business One with OptiProERP targets the six critical operational areas central to any manufacturing companyutomates and streamlines business processes, so your business runs faster and
						more profitabl</p>
					</div>
					<div class="col-lg-3 no-padding">
						<div class="col-lg-12 web1 font1 b-r b-t">
							
							<img src="img/ad1.png">
							<h5>Simpler Processes</h5>
						</div>
						<div class="col-lg-12  web1 font1 b-r  ">

							<img src="img/ad2.png">
							<h5> Easy Maintenance</h5>
						</div>

					</div>
					
					<div class="col-lg-3 no-padding ">
						<div class="col-lg-12  web1 font1 b-r b-t ">

							<img src="img/ad3.png">
							<h5>Scalable Solution</h5>
						</div>
						
						<div class="col-lg-12  web1 font1 b-r  ">

							<img src="img/ad4.png">
							<h5>Quicker Deployment</h5>
						</div>
						
					</div>

					<div class="clearfix"></div>

					<div class="col-lg-12 ">
						<p class="decr">Our ERP system works seamlessly with SAP Business One to manage business processes like Production, Supply Chain Planning and Quality Control.he next generation ERP Software, helps you capitalize by unifying traditional manufacturing capabilities into a new purpose-built solutionSAP Busin
						more profitabl</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

			
		</div>
		
	</div>
	

	
	

	<div class="clearfix"></div>

	
</main>
	<a href="{{ URL::to('/portfolio') }}"><div class="ban1">
		<img style="cursor:pointer;" class="img3" src="{{ asset('img/ban1.png') }}">

	</div></a>




	<div class="other-ser">

		<div class="container">


			<div class="title">
				<h2>OTHER SERVICES</h2>
			</div>
			
			<a href="{{ asset('search-engine-optimization') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a2.png') }}">
				<h4>SEO</h4>
			</div></a>
			
			<a href="{{ asset('smo') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a3.png') }}">
				<h4>SMO</h4>
			</div></a>
			<a href="{{ asset('mobile-application') }}"><div class="col-lg-2 col-xs-6 b-r">
				
				<img src="{{ asset('img/a4.png') }}">
				<h4>Android Application </h4>
			</div></a>
			
			
			<a href="{{ asset('website-design-company') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a7.png') }}">
				<h4>Web Design</h4>
			</div></a>
			<a href="{{ asset('web-development') }}">
				<div class="col-lg-2 col-xs-6  "><img src="{{ asset('img/a1.png') }}"><h4>Web Development</h4></div></a>
			
			<a href="{{ asset('e-commerce') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a6.png') }}">
				<h4>E-commerce</h4>
			</div></a>
		</div>
	</div>
	<div class="text-center en">
    <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary open-switcher show-switcher"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
    <h5>+91 7020914154</h5>
    <h5>sales@sungare.com</h5>
</div>
	@endsection
	@push('footer')
	
	@endpush