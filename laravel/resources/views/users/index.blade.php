@extends('users.layouts.master')

@push('header')

@endpush

@section('content')
<div class="main-bg">
		<img src="{{ asset('img/005.jpg') }}">
	</div>
	<div id="jssor_1"  class="js1">
		<!-- Loading Screen -->
		<div class=" banner-head">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="block">
							<h1>SUNGARE</h1>
							<h4>Looking for the world's </br>
							Best Websites? It's right here.</h4>
						</div>
					</div>
				</div>
				<div class="scrolldown">
					<a id="scroll" href="#features" class="scroll"></a>
				</div>
			</div>
		</div>
		<div data-u="slides" class="slide1" >
			<div>
				<img data-u="image" src="{{ asset('img/001.jpg') }}" />
			</div>
			<div>
				<img data-u="image" src="{{ asset('img/002.jpg') }}" />
			</div>
			<div>
				<img data-u="image" src="{{ asset('img/001.jpg') }}" />
			</div>
			<div>
				<img data-u="image" src="{{ asset('img/002.jpg') }}" />
			</div>
		</div>
	</div>
	<div class="project section">
		<div class="container">
				<!-- Start Heading -->
			<div class="title">
				<h2>WHAT WE DO ?</h2>
			</div>
				
			<div class="col-lg-8 ser no-p ">
				<div class="col-lg-4 no-padding">
					<a href="{{ URL::to('/web-development') }}"><div class="col-lg-12 featured-content service1 font1">
						Application Development
					</div></a>
					<a href="{{ URL::to('/e-commerce') }}"><div class="col-lg-12 featured-content service2 font1">
						E-commerce
					</div></a>

				</div>
				<a href="{{ URL::to('/website-design-company') }}"><div class="col-lg-4 featured-content service3 font1">
					Web Design
				</div></a>
				<div class="col-lg-4 no-padding ">
					<a href="{{ URL::to('/search-engine-optimization') }}"><div class="col-lg-12 featured-content service4 font1">
						Digital marketing
					</div></a>
					<a href="{{ URL::to('/mobile-application') }}"><div class="col-lg-12 featured-content service5 font1">
						Mobile Application
					</div></a>
				</div>
			</div>

			<div class="col-lg-4 no-p">
				<div class="text-of-problems">
					<h3 class="block-title">Integrate systems for smarter data</h3>
					
					<ul class="list-with-line">
						<li>Connect online, shops, and warehouses with live stock data</li>
						<li>Send sales metrics to your finance tools for greater insights</li>
						<li>Run targetted marketing campaigns with customer profiling</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x bg3" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
		<div class="testimonial4_header no-p">
			<h4>what our clients are saying</h4>
		</div>
		<ol class="carousel-indicators">
			<li data-target="#testimonial4" data-slide-to="0" class="active"></li>
			<li data-target="#testimonial4" data-slide-to="1"></li>
			<li data-target="#testimonial4" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="testimonial4_slide">
					<div class="test-left col-lg-4 "><img src="{{ asset('images/1.png') }}" class="img-responsive" />
						<h4>Ben Hanna</h4>
						<span>UI Developer</span>
					</div>
					<div class="test-left col-lg-8">
						<p class="desc"> Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed eratorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis ar in in velit quis arcu ornare laoreet urabitur " .</p>
						
					</div>
				</div>
			</div>
			<div class="item">
				<div class="testimonial4_slide">
					<div class="test-left col-lg-4 "><img src="{{ asset('images/1.png') }}" class="img-responsive" />
						<h4>Ben Hanna</h4>
						<span>UI Developer</span>
					</div>
					<div class="test-left col-lg-8">
						<p class="desc"> Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur." </p>
						
					</div>
				</div>
			</div>
			<div class="item">
				<div class="testimonial4_slide">
					<div class="test-left col-lg-4 ">
					<img src="{{ asset('images/1.png') }}" class="img-responsive" />
					<h4>Ben Hanna</h4>
					<span>UI Developer</span></div>
					<div class="test-left col-lg-8">
						<p class="desc">  Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed eratorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis ar in in velit quis arcu ornare laoreet urabitur ".</p>
						
					</div>
				</div>
			</div>
		</div>
		<a class="left carousel-control" href="#testimonial4" role="button" data-slide="prev">
			<span class="fa fa-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#testimonial4" role="button" data-slide="next">
			<span class="fa fa-chevron-right"></span>
		</a>
	</div>
	<div style="background: #fff;" class="project section">
		<div class="project section">
			<div class="container">
					<!-- Start Heading -->
					
			<!-- Start Heading -->
				<div class="title">
					<h2>OUR WORKS</h2>
				</div>
				@foreach($portfolio as $row)
				<div class="col-md-4 col-sm-4 col-xs-12 portmain">
					<a href="{{$row->tPortfolioUrl}}" target="_blank">
						<img src="{{$row->tPortfolioImage}}" class="img-responsive" style=" margin:0 auto;">
						<p class="porttext">  
							{{$row->vPortfolioName}}
						</p>
						
					</a>
				</div>
				@endforeach
				<div class="clearfix"></div>
				<div class="text-center">
					<a href="{{{URL::to('/portfolio')}}}"><button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary">VIEW MORE</button></a>
				</div>
			</div>
		</div>

		<div class="text-center en" style="    background:   ;">
			<button type="submit" value="SEND" class="btn btn-lg btn-view btn-primary open-switcher show-switcher"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
			<h5>+91 7020914154</h5>
			<h5>sales@sungare.com</h5>

		</div>
	</div>

@endsection

@push('footer')

@endpush