@extends('users.layouts.master')

@section('content')
<section class="headersec" id="banner3">
		
		<div class="header">
			<div class="container">
				<div class="logo">
					<a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo7.png') }}"></a>
				</div> 

				<div class="col-lg-8 col-md-6 col-xs-5 col-sm-6 mt">
					<div class="widget-icon">
						<a href="https://www.facebook.com/Creativity.Redefined/"  target="_blank"><i class="fa fa-facebook"></i></a>
						
						<a href="https://in.linkedin.com/company/sungare-technologies-pvt-ltd" 	target="_blank"><i class="fa fa-linkedin" 
							></i></a>
							
					</div>
				</div>
				<div class="menu">

					<div class="one"><span>MENU</span></div>
					<div class="two"><a href="#" class="navicon"></a></div> 
					<div class="toggle effect-3"> 
						<ul class="toggle-menu">
							<li><a href="{{ URL::to('/') }}" {{ ($pagename=='home')?"class=active":'' }}> Home</a></li>
							
							<li><a href="{{ URL::to('/product-service') }}" {{ ($pagename=='product-service')?"class=active":'' }}> Services</a></li>
							<li ><a href="{{ URL::to('/portfolio') }}" {{ ($pagename=='portfolio')?"class=active":'' }}>Our Works</a></li> 
							<li><a href="{{ URL::to('/our-team') }}" {{ ($pagename=='ourteam')?"class=active":'' }}> Our Team</a></li>						
							<li><a href="{{ URL::to('/career') }}" {{ ($pagename=='career')?"class=active":'' }}>Career</a></li>					
							<li><a href="{{ URL::to('/contact-us') }}" {{ ($pagename=='contactus')?"class=active":'' }}> Contact Us</a></li>
						</ul>
					</div> 
				</div>   
					
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="clearfix"> </div>
		
		<div class="title">
			<h2 style="padding-left: 10px;">Web Application Development</h2>
		</div>

	</section>

<main role="main-home-wrapper" class="container sungare-web-design no-p">
	<h2 class="block-title">Web apps are more<span class="color-red"> powerful</span> than ever!</h2>

	<div class="col-xs-12 no-p">
		<div class="col-lg-6">

			<p class="decr">Web application acts like the backbone of an enterprise and is extremely essential for supporting nearly every aspect of how online business is planned, executed and managed. Having the vast experience, our team of skillful developers helps you plan, discover & implement critical WebApps that have become a competitive asset for your online business processes. Being established as a prominent Custom WebApps service provider, we put our 100% diligence in creating entirely unique yet innovative web applications that are capable of handling huge amount of data as well as complex transactions while keeping security and performance in mind. </p>
		</div>
		

		
		<div class="col-lg-6">
			<div class="col-lg-12 ser no-p">
				
				<img  class="img3"  src="{{ asset('img/wa.png') }}">
				
			</div>
		</div>

		<div class="clearfix"></div>
		<div class="col-lg-12 no-p ">
			<div class="col-lg-6 b-r">
				<ul class="list-with-line">
					<li>Connect online, shops, and warehouses with , and warehouses with live, and warehouses with live live stock data</li>
					<li>Send sales metrics to your f, and warehouses with live, and warehouses with live, and warehouses with liveinance tools for greater insights</li>
					<li>Run targetted marketing c, and warehouses with live, and warehouses with live, and warehouses with liveampaigns with customer profiling</li>
					
				</ul>
			</div>
			<div class="col-lg-6">
				<ul class="list-with-line">
					<li>Connect online, shops, and warehouses with , and warehouses with live, and warehouses with live live stock data</li>
					<li>Send sales metrics to your f, and warehouses with live, and warehouses with live, and warehouses with liveinance tools for greater insights</li>
					<li>Run targetted marketing c, and warehouses with live, and warehouses with live, and warehouses with liveampaigns with customer profiling</li>
					
				</ul>
			</div>

			<div class="clearfix"></div>

			
		</div>
		
	</main>
	<a href="{{ URL::to('/portfolio') }}"><div class="ban1">
		<img style="cursor:pointer;" class="img3" src="{{ asset('img/ban1.png') }}">

	</div></a>




	<div class="other-ser">

		<div class="container">


			<div class="title">
				<h2>OTHER SERVICES</h2>
			</div>
			
			<a href="{{ asset('search-engine-optimization') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a2.png') }}">
				<h4>SEO</h4>
			</div></a>
			
			<a href="{{ asset('smo') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a3.png') }}">
				<h4>SMO</h4>
			</div></a>
			<a href="{{ asset('mobile-application') }}"><div class="col-lg-2 col-xs-6 b-r">
				
				<img src="{{ asset('img/a4.png') }}">
				<h4>Android Application </h4>
			</div></a>
			<a href="{{ asset('erp') }}"><div class="col-lg-2 col-xs-6 b-r">
				<img src="{{ asset('img/a5.png') }}">
				<h4>ERP Development</h4>
			</div></a>
			
			<a href="{{ asset('website-design-company') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a7.png') }}">
				<h4>Web Design</h4>
			</div></a>
			
			<a href="{{ asset('e-commerce') }}"><div class="col-lg-2 col-xs-6  ">
				<img src="{{ asset('img/a6.png') }}">
				<h4>E-commerce</h4>
			</div></a>
		</div>
	</div>
	<div class="text-center en">
    <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-view btn-primary open-switcher show-switcher"><b style="color: #fff;">Enquire </b> <b style="color: #e04d00;"> Now </b></button>
    <h5>+91 7020914154</h5>
    <h5>sales@sungare.com</h5>
</div>
	@endsection
	@push('footer')
	<script type="text/javascript">
		
	</script>
	@endpush