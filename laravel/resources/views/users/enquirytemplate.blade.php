<h1>Enquiry Details</h1>
<h2>Enquiry For: {{ $service_name }}</h2>
<p>Client Name: {{ $client_name }}</p>
<p>Client Email: {{ $client_email }}</p>
<p>Client Mobile No : {{ $client_phone_num }}</p>
<p>Client Message : {{ $client_message }}</p>