@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Job Skills</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
						<li class="breadcrumb-item active">Job Skills</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<form action="{{ URL::to('/admin/jobskills/delete') }}" method="post" name="catlist_form" id="joblist_form">
			<input name="_token" type="hidden" value="{{ csrf_token() }}" />
			<div class="row">
			
				<div class="col-sm-12" style="padding-bottom: 10px;">
					<div class="col-sm-3">
					</div>
					<div class="col-sm-3">
					</div>
					<div class="col-sm-2">
					</div>
						<div class="col-sm-2" style="float: right;">
							<button type="button" class="btn btn-block btn-primary" onclick="redirectURL('/admin/jobskills/add')">Add</button>
						</div>
						<div class="col-sm-2" style="padding-right: 10px;">
							<input type="button" class="btn btn-block btn-danger delete" value="Delete">
						</div>
				</div>
				@if(Session::has('message'))
					<div class="col-12">
						<div class="alert alert-success alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  
		                  {{ Session::get('message') }}
		                </div>
					</div>
				@endif
				<div class="col-12">
				<!-- /.card -->

					<div class="card">
						<div class="card-header">
							<h3 class="card-title">List Of Job Skills Require</h3>
						</div>
						<!-- /.card-header -->

						<div class="card-body">
								<table id="list_Job_Descriptions" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="5%"><center><input type="checkbox" name="check_all[]" id="check_all"></center></th>
											<th width="20%">Category</th>
											<th width="20%">Location</th>
											<th width="50%">Job Skills</th>
											<th width="5%"><center>Edit</center></th>
										</tr>
									</thead>
									<tbody>
										@foreach($all_job as $key=>$values)
											<tr>
												<td><center><input type="checkbox" name="id[]" class="id" value="{{ $values['job_skills'] }}"></center></td>
												<td>{{ $values['cat_name'] }}</td>
												<td>{{ $values['city_name'] }}</td>
												<td>{{ str_limit($values['skill_content'], 50) }}</td>
												<td><center><a href="{{ URL::to('/admin/job/edit/'.$values["jobdesc_id"]) }}"><i class="nav-icon fa fa-edit"></i></a></center></td>
											</tr>
										@endforeach
									</tbody>
								</table>
						</div>
						<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
			
			<!-- /.col -->
			</div>
		</form>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$("#check_all").click(function () {

            $(".id").prop('checked', $(this).prop('checked'));
        });
	 });
	$(".delete").click(function(){
	   if($('[name="id[]"]:checked').length==0)
	   {
	   		$('.modal-title').html('Error Message');
	   		$('.modal-body').html('Please Select atleast One Record !!');
	   		$("#myModal").modal({show: true});
	   }
	   else
	   {
	   		$( "#joblist_form" ).submit();
	   }
	});
	setTimeout(function() {
		$(".alert").hide()
	}, 5000);
	$(function () {
	    $('#list_Job_Descriptions').DataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            { "bSortable": false },
	            null,
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
  	});
</script>
@endsection