@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Cities</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
						<li class="breadcrumb-item active">Cities</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<form action="{{ URL::to('/admin/city/delete') }}" method="post" name="catlist_form" id="catlist_form">
			<input name="_token" type="hidden" value="{{ csrf_token() }}" />
			<div class="row">
			
				<div class="col-sm-12" style="padding-bottom: 10px;">
					<div class="col-sm-3">
					</div>
					<div class="col-sm-3">
					</div>
					<div class="col-sm-2">
					</div>
						<div class="col-sm-2" style="float: right;">
							<button type="button" class="btn btn-block btn-primary" onclick="redirectURL('/admin/city/add')">Add</button>
						</div>
						<div class="col-sm-2" style="padding-right: 10px;">
							<input type="button" class="btn btn-block btn-danger delete" value="Delete">
						</div>
				</div>
				@if(Session::has('message'))
					<div class="col-12">
						<div class="alert alert-success alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  
		                  {{ Session::get('message') }}
		                </div>
					</div>
				@endif
				<div class="col-12">
				<!-- /.card -->

					<div class="card">
						<div class="card-header">
							<h3 class="card-title">List Of Cities</h3>
						</div>
						<!-- /.card-header -->

						<div class="card-body">
								<table id="list_Cities" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="10%"><center><input type="checkbox" name="check_all[]" id="check_all"></center></th>
											<th width="20%">Title</th>
											<th width="10%"><center>Edit</center></th>
										</tr>
									</thead>
									<tbody>
										@foreach($all_city as $key=>$values)
											<tr>
												<td><center><input type="checkbox" name="id[]" class="id" value="{{ $values['city_id'] }}"></center></td>
												<td>{{ $values['city_name'] }}</td>
												<td><center><a href="{{ URL::to('/admin/city/edit/'.$values["city_id"]) }}"><i class="nav-icon fa fa-edit"></i></a></center></td>
											</tr>
										@endforeach
									</tbody>
								</table>
						</div>
						<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
			
			<!-- /.col -->
			</div>
		</form>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$("#check_all").click(function () {

            $(".id").prop('checked', $(this).prop('checked'));
        });
	 });
	$(".delete").click(function(){
	   if($('[name="id[]"]:checked').length==0)
	   {
	   		$('.modal-title').html('Error Message');
	   		$('.modal-body').html('Please Select atleast One Record !!');
	   		$("#myModal").modal({show: true});
	   }
	   else
	   {
	   		$( "#catlist_form" ).submit();
	   }
	});
	setTimeout(function() {
		$(".alert").hide()
	}, 5000);
	$(function () {
	    $('#list_Cities').DataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            { "bSortable": false },
	            null,
	           	{ "bSortable": false },
            ]

	    });
  	});
</script>
@endsection