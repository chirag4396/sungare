@extends('backend.layouts.master')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<div class="container-fluid">
         	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h1>Job Description</h1>
          		</div>
          		<div class="col-sm-6">
	            	<ol class="breadcrumb float-sm-right">
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/job') }}">Job Description</a></li>
	              		<li class="breadcrumb-item active">{{ $mode }} Job Description</li>
	            	</ol>
          		</div>
        	</div>
      	</div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
	    <div class="row">
	      <!-- left column -->
	      	<div class="col-md-12">
	        <!-- general form elements -->
		      	<div class="card card-primary">
		          	<div class="card-header">
		            	<h3 class="card-title">{{ $mode }} Job Description</h3>
		        	</div>
		        	@php $url=($mode=='Add') ? '/admin/job/add': '/admin/job/edit/'.$job->jobdesc_id;
		        	@endphp
		        	@if (count($errors)) 
				      <div class="alert alert-danger error-box" id="error-box" style="margin-top: 10px;">
							<ul>
					       		@foreach($errors->all() as $error) 
					        
					            	<li>{{ $error }}</li>
					          
								@endforeach 
							</ul>
					 	</div>    
					@endif
		        	<form role="form" enctype="multipart/form-data" action="{{ URL::to($url) }}" method="post" name="add_update_portfolio">
		        		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		        		<div class="card-body">
		        			<div class="form-group">
		        				<label for="exampleInputEmail1">Job Category</label>
			        				<select class="form-control" name="cat_id">
			        					<option value=''>Choose Job Category</option>
			        					@foreach($categories as $value)
			        							<option value="{{ $value->cat_id }}"  @if($mode=='Update'){{ ( $value->cat_id == $job->cat_id ) ? 'selected' : '' }} @endif>{{ $value->cat_name }}</option>
			        					@endforeach
			        				</select>
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputEmail1">Job Locations</label>
			        				<select class="form-control" name="city_id[]" multiple="">
			        					<option value="">Choose Job Location</option>
			        					@foreach($locations as $value)
			        							<option value="{{ $value->city_id }}"  @if($mode=='Update'){{ in_array($value->city_id, $locations_selected) ? 'selected' : '' }} @endif>{{ $value->city_name }}</option>
			        					@endforeach
			        				</select>
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputPassword1">Job Description</label>
		        				<textarea class="form-control job_description" id="job_description" placeholder="Job Description" name="job_description">{{ ($mode=='Update') ? $job->job_description:'' }}</textarea>
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputPassword1">Job Skill Required</label>
		        				<textarea class="form-control skill_content" id="skill_content" placeholder="Job Skill Required" name="skill_content">{{ ($mode=='Update') ? $job->skill_content:'' }}</textarea>
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputPassword1">Job Candidate Profile</label>
		        				<textarea class="form-control candidate_profile" id="candidate_profile" placeholder="Job Description" name="candidate_profile">{{ ($mode=='Update') ? $job->candidate_profile:'' }}</textarea>
		        			</div>
		        		</div>
		        		<!-- /.card-body -->

		        		<div class="card-footer">
		        			<input type="submit" class="btn btn-primary" value="{{ $mode }}" name="submit">
		        			<input type="button" class="btn btn-danger" value="Cancel" onclick="redirectURL('/admin/job')">
		        		</div>
		        	</form>
		        </div>
		    </div>
		</div>
	</div>
</div>

<script type="text/javascript">
	setTimeout(function() {
		$("#error-box").hide()
	}, 5000);
	

	CKEDITOR.replace( 'skill_content' );
	CKEDITOR.replace( 'candidate_profile' ); 
	CKEDITOR.replace( 'job_description' );  
	//CKEDITOR.replace('#skill_content');
</script>
@endsection