@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<div class="container-fluid">
         	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h1>Portfolio</h1>
          		</div>
          		<div class="col-sm-6">
	            	<ol class="breadcrumb float-sm-right">
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/portfolio') }}">Portfolio</a></li>
	              		<li class="breadcrumb-item active">{{ $mode }} Portfolio</li>
	            	</ol>
          		</div>
        	</div>
      	</div><!-- /.container-fluid -->
    </section>
    @if ($errors->any())

    @endif
    <div class="container-fluid">
	    <div class="row">
	      <!-- left column -->
	      	<div class="col-md-12">
	        <!-- general form elements -->
		      	<div class="card card-primary">
		          	<div class="card-header">
		            	<h3 class="card-title">{{ $mode }} Portfolio</h3>
		        	</div>
		        	@php $url=($mode=='Add') ? '/admin/portfolio/store': '/admin/portfolio/edit/'.$portfolio->iPortfolioId;
		        	@endphp
		        	@if (count($errors)) 
				      <div class="alert alert-danger error-box" id="error-box" style="margin-top: 10px;">
							<ul>
					       		@foreach($errors->all() as $error) 
					        
					            	<li>{{ $error }}</li>
					          
								@endforeach 
							</ul>
					 	</div>    
					@endif
		        	<form role="form" enctype="multipart/form-data" action="{{ URL::to($url) }}" method="post" name="add_update_portfolio">
		        		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		        		<div class="card-body">
		        			<div class="form-group">
		        				<label for="exampleInputEmail1">Portfolio Type</label>
			        				<select class="form-control" name="portfoliotype_id">
			        					<option>Choose Portfolio type</option>
			        					@foreach($portfoliotypes as $value)
			        							<option value="{{ $value->portfoliotype_id }}"  @if($mode=='Update') {{ ($value->portfoliotype_id == $portfolio->portfoliotype_id) ? 'selected' : '' }}@endif>{{ $value->portfoliotype_name }}</option>
			        					@endforeach
			        				</select>
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputPassword1">Portfolio Name</label>
		        				<input type="text" class="form-control" id="vPortfolioName" placeholder="Portfolio Name" name="vPortfolioName" value="{{ ($mode=='Update') ? $portfolio->vPortfolioName:'' }}">
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputPassword1">Portfolio Url</label>
		        				<input type="text" class="form-control" id="tPortfolioUrl" placeholder="Portfolio Url" name="tPortfolioUrl" value="{{ ($mode=='Update') ? $portfolio->tPortfolioUrl:'' }}">
		        			</div>
		        			<div class="form-group">
		        				<label for="exampleInputFile">Portfolio Image</label>
		        				<div class="input-group">
		        					@if($mode=='Update')
		        						<div class="col-sm-12">
			        						<img src="{{ $portfolio->tPortfolioImage }}" width="200px" height="200px" style="border: 1px solid red">
			        					</div>
			        					<div class="col-sm-12" style="margin-top: 10px;">
			        						<input type="file" class="custom-file-input form-control" id="tPortfolioImage" name="tPortfolioImage" accept="image/*>
		        							<label class="custom-file-label" for="exampleInputlabel">Change Image</label>
			        					</div>
		        					@else
		        					<div class="custom-file">
		        						<input type="file" class="custom-file-input form-control" id="tPortfolioImage" name="tPortfolioImage">
		        						<label class="custom-file-label" for="exampleInputlabel">Choose Image</label>
		        					</div>
		        					@endif
		        				</div>
		        			</div>
		        		</div>
		        		<!-- /.card-body -->

		        		<div class="card-footer">
		        			<input type="submit" class="btn btn-primary" value="{{ $mode }}" name="submit">
		        			<input type="button" class="btn btn-danger" value="Cancel" onclick="redirectURL('/admin/portfolio')">
		        		</div>
		        	</form>
		        </div>
		    </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	setTimeout(function() {
		$("#error-box").hide()
	}, 5000);
</script>
@endsection