@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Enquiries</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
						<li class="breadcrumb-item active">Enquiries</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<form action="{{ URL::to('/admin/enquiries/delete') }}" method="post" name="enquirylist" id="enquirylist">
			<input name="_token" type="hidden" value="{{ csrf_token() }}" />
			<div class="row">
			
				<div class="col-sm-12" style="padding-bottom: 10px;">
					<div class="col-sm-3">
					</div>
					<div class="col-sm-3">
					</div>
					<div class="col-sm-2">
					</div>
						<div class="col-sm-2" style="padding-right: 10px;float: right;">
							<input type="button" class="btn btn-block btn-danger delete" value="Delete">
						</div>
				</div>
				@if(Session::has('message'))
					<div class="col-12">
						<div class="alert alert-success alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  {{ Session::get('message') }}
		                </div>
					</div>
				@endif
				<div class="col-12">
				<!-- /.card -->

					<div class="card">
						<div class="card-header">
							<h3 class="card-title">List Of Enquiries</h3>
						</div>
						<!-- /.card-header -->

						<div class="card-body">
								<table id="list_enquiry" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="3%"><center><input type="checkbox" name="check_all[]" id="check_all"></center></th>
											<th width="15%">Enquiry For</th>
											<th width="20%">Client Name</th>
											<th width="20%">Client Email</th>
											<th width="20%">Client Phone No.</th>
											<th width="40%">Client Message / Comments</th>
										</tr>
									</thead>
									<tbody>
										@foreach($all_enquiries as $key=>$values)
											<tr>
												<td><center><input type="checkbox" name="id[]" class="id" value="{{ $values['enquiry_id'] }}"></center></td>
												<td>{{ $values['service_name'] }}</td>
												<td>{{ $values['client_name'] }}</td>
												<td>{{ $values['client_email'] }}</td>
												<td>{{ $values['client_phone_num'] }}</td>
												<td><center><input type="button" class="btn btn-info" style="border-radius: 16px;" onclick="commentview({{ $values['enquiry_id'] }})" value="View"><input type="button" class="btn btn-warning" style="border-radius: 16px;margin-left: 10px;" onclick="redirectURL('/admin/enquiries/addcomment/{{ $values['enquiry_id'] }}')" value="Add Comment"></center></td>
											</tr>
										@endforeach
									</tbody>
								</table>
						</div>
						<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
			
			<!-- /.col -->
			</div>
		</form>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$("#check_all").click(function () {

            $(".id").prop('checked', $(this).prop('checked'));
        });
	 });
	$(".delete").click(function(){
	   if($('[name="id[]"]:checked').length==0)
	   {
	   		$('.modal-title').html('Error Message');
	   		$('.modal-body').html('Please Select atleast One Record !!');
	   		$("#myModal").modal({show: true});
	   }
	   else
	   {
	   		$( "#enquirylist" ).submit();
	   }
	});
	function commentview(id)
	{
		
		var ajaxURL="{{ URL::to('/enquiries/commentview') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            data:{'id':id},
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){

            	$('.modal-title').html('Message');
	   			$('.modal-body').html(data);
	   			$("#myModal").modal({show: true});
            	//$('#myModal').modal();
            }
         });
	}
	setTimeout(function() {
		$(".alert").hide()
	}, 5000);
	$(function () {
	    $('#list_enquiry').DataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            { "bSortable": false },
	            null,
	            null,
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
  	});
</script>
@endsection