@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<div class="container-fluid">
         	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h1>Categories</h1>
          		</div>
          		<div class="col-sm-6">
	            	<ol class="breadcrumb float-sm-right">
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/category') }}">Categories</a></li>
	              		<li class="breadcrumb-item active">{{ $mode }} Categories</li>
	            	</ol>
          		</div>
        	</div>
      	</div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
	    <div class="row">
	      <!-- left column -->
	      	<div class="col-md-12">
	        <!-- general form elements -->
		      	<div class="card card-primary">
		          	<div class="card-header">
		            	<h3 class="card-title">{{ $mode }} Category</h3>
		        	</div>
		        	@php $url=($mode=='Add') ? '/admin/category/add': '/admin/category/edit/'.$category->cat_id;
		        	@endphp
		        	@if (count($errors)) 
				      <div class="alert alert-danger error-box" id="error-box" style="margin-top: 10px;">
							<ul>
					       		@foreach($errors->all() as $error) 
					        
					            	<li>{{ $error }}</li>
					          
								@endforeach 
							</ul>
					 	</div>    
					@endif
		        	<form role="form" enctype="multipart/form-data" action="{{ URL::to($url) }}" method="post" name="add_update_category">
		        		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		        		<div class="card-body">
		        			<div class="form-group">
		        				<label for="exampleInputPassword1">Title</label>
		        				<input type="text" class="form-control" id="portfoliotype_name" placeholder="Category name" name="cat_name" value="{{ ($mode=='Update') ? $category->cat_name:'' }}">
		        			</div>
		        			
		        		</div>
		        		<div class="card-footer">
		        			<input type="submit" class="btn btn-success" value="{{ $mode }}" name="submit">
		        			<input type="button" class="btn btn-danger" value="Cancel" onclick="redirectURL('/admin/category')">
		        		</div>
		        	</form>
		        </div>
		    </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	setTimeout(function() {
		$("#error-box").hide()
	}, 5000);
</script>
@endsection