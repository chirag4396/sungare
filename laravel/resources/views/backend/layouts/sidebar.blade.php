
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="" class="brand-image img-circle elevation-3"
    style="opacity: .8">
        <span class="brand-text font-weight-light">Sungare Technologies</span>
    </a>

<!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('backend/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

    <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="#" class="nav-link {{ ($pagename=='dashboard') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ URL::to('/admin/portfolio') }}" class="nav-link {{ ($pagename=='portfolio') ? 'active' : '' }}">
                          <i class="nav-icon fa fa-th"></i>
                          <p>
                            Portfolio
                        </p>
                    </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ URL::to('/admin/portfoliotype') }}" class="nav-link {{ ($pagename=='portfoliotype') ? 'active' : '' }}">
                          <i class="nav-icon fa fa-pie-chart"></i>
                          <p>
                            Portfolio Type
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ URL::to('/admin/enquiries') }}" class="nav-link {{ ($pagename=='enquiry') ? 'active' : '' }}">
                      <i class="nav-icon fa fa-tree"></i>
                      <p>
                        Enquiries
                    </p>
                </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ URL::to('/admin/services') }}" class="nav-link {{ ($pagename=='services') ? 'active' : '' }}">
                      <i class="nav-icon fa fa-edit"></i>
                      <p>
                        Services
                    </p>
                </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ URL::to('/admin/category') }}" class="nav-link {{ ($pagename=='category') ? 'active' : '' }}">
                      <i class="nav-icon fa fa-edit"></i>
                      <p>
                        Job Categories
                    </p>
                </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ URL::to('/admin/city') }}" class="nav-link {{ ($pagename=='city') ? 'active' : '' }}">
                      <i class="nav-icon fa fa-edit"></i>
                      <p>
                        Cities
                    </p>
                </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ URL::to('/admin/job') }}" class="nav-link {{ ($pagename=='job') ? 'active' : '' }}">
                      <i class="nav-icon fa fa-edit"></i>
                      <p>
                        Career Jobs
                    </p>
                </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ URL::to('/admin/jobenquiry') }}" class="nav-link {{ ($pagename=='jobenquiry') ? 'active' : '' }}">
                      <i class="nav-icon fa fa-edit"></i>
                      <p>
                        Job Enquiries
                    </p>
                </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                      <i class="nav-icon fa fa-edit"></i>
                      <p>
                        Signout
                    </p>
                </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>