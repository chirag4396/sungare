@include('backend.layouts.common_css')
@include('backend.layouts.common_js')
@include('backend.layouts.header')

@include('backend.layouts.sidebar')

@yield('content')

@include('backend.layouts.footer')
@include('backend.layouts.modal')

