@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Portfolio</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
						<li class="breadcrumb-item active">Portfolio</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<form action="{{ URL::to('/admin/portfolio/delete') }}" method="post" name="portfolio" id='portfoliolist_form'>
		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		<div class="row">
			<div class="col-12">
				<div class="col-sm-2" style="float: right;padding-bottom: 10px;">
					<button type="button" class="btn btn-block btn-primary" onclick="redirectURL('/admin/portfolio/add')">Add</button>

				</div>
				<div class="col-sm-2" style="padding-right: 10px;">
					<input type="button" class="btn btn-block btn-danger delete" value="Delete">
				</div>
			</div>
			@if(Session::has('message'))
				<div class="col-12">
					<div class="alert alert-success alert-dismissible">
	                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                  
	                  {{ Session::get('message') }}
	                </div>
				</div>
			@endif
			<div class="col-12">
				<!-- /.card -->

				<div class="card">
					<div class="card-header">
						<h3 class="card-title">List Of Portfolio</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<table id="list_portfolio" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th width="10%"><center><input type="checkbox" name="check_all[]" id="check_all"></center></th>
									<th width="20%">Image</th>
									<th width="20%">Type</th>
									<th width="40%">Name</th>
									<th width="10%"><center>Edit</center></th>
								</tr>
							</thead>
							<tbody>
								@foreach($all_portfolio as $key=>$values)
									<tr>
										<td><center><input type="checkbox" name="id[]" id="id" value="{{ $values['iPortfolioId'] }}"></center></td>
										<td><img src="{{ $values['tPortfolioImage'] }}" width="200px" height="100px"></td>
										<td>{{ $values['portfoliotype'] }}</td>
										<td>{{ $values['vPortfolioName'] }}</td>
										<td><center><a href="{{ URL::to('/admin/portfolio/edit/'.$values["iPortfolioId"]) }}"><i class="nav-icon fa fa-edit"></i></a></center></td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
		</form>
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$("#check_all").click(function () {

            $(".id").prop('checked', $(this).prop('checked'));
        });
	 });
	$(".delete").click(function(){
	   if($('[name="id[]"]:checked').length==0)
	   {
	   		$('.modal-title').html('Error Message');
	   		$('.modal-body').html('Please Select atleast One Record !!');
	   		$("#myModal").modal({show: true});
	   }
	   else
	   {
	   		$( "#portfoliolist_form" ).submit();
	   }
	});
	setTimeout(function() {
		$(".alert").hide()
	}, 5000);
	$(function () {
		$('#list_portfolio').dataTable().fnDestroy();
	    $('#list_portfolio').dataTable({
            "bAutoWidth": false,
            "aaSorting": [],
            "aoColumns": [
	            { "bSortable": false },
	            { "bSortable": false },
	            null,
	            null,
	           	{ "bSortable": false },
            ]
        });
   
  	});
</script>
@endsection