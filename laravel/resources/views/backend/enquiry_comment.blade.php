@extends('backend.layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<div class="container-fluid">
         	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h1>Enquiry</h1>
          		</div>
          		<div class="col-sm-6">
	            	<ol class="breadcrumb float-sm-right">
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/dashboard') }}">Dashboard</a></li>
	              		<li class="breadcrumb-item"><a href="{{ URL::to('/admin/enquiries') }}">Enquiry</a></li>
	              		<li class="breadcrumb-item active">Add Comment</li>
	            	</ol>
          		</div>
        	</div>
      	</div><!-- /.container-fluid -->
    </section>
    @if ($errors->any())

    @endif
    <div class="container-fluid">
	    <div class="row">
	      <!-- left column -->
	      	<div class="col-md-12">
	        <!-- general form elements -->
		      	<div class="card card-primary">
		          	<div class="card-header">
		            	<h3 class="card-title">Add Comment</h3>
		        	</div>
		        	<form role="form" enctype="multipart/form-data" action="{{ URL::to('/admin/enquiries/addcomment/'.$enquiry['enquiry_id']) }}" method="post" name="add_update_portfolio">
		        		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		        		<div class="card-body">
		        			<div class="row">
		        				<div class="col-sm-4">
		        					<div class="form-group">
			        					<label for="exampleInputEmail1">Client Name</label>
				        				<input type="text" class="form-control" id="vPortfolioName" placeholder="" name="vPortfolioName" value="{{ $enquiry['client_name'] }}" disabled="">	
				        			</div>
			        			</div>
			        			<div class="col-sm-4">
			        				<div class="form-group">
			        					<label for="exampleInputPassword1">Client Email</label>
			        					<input type="text" class="form-control" id="client_email" placeholder="" name="client_email" value="{{ $enquiry['client_email'] }}" disabled="">
			        				</div>
			        			</div>
			        			<div class="col-sm-4">
			        				<div class="form-group">
			        					<label for="exampleInputPassword1">Client Phone No.</label>
			        					<input type="text" class="form-control" id="client_phone_num" placeholder="Portfolio Url" name="client_phone_num" value="{{ $enquiry['client_phone_num']}}" disabled="">
			        				</div>
			        			</div>
			        			
			        		</div>
			        		<div class="row">
			        			<div class="col-sm-12">
			        				<div class="form-group">
			        					<label for="exampleInputPassword1">Client Message</label>
			        					<textarea class="form-control" name="client_message" disabled="">{{ $enquiry['client_message'] }}</textarea>
			        				</div>
				        		</div>
				        		<div class="col-sm-12">
			        				<div class="form-group">
			        					<label for="exampleInputPassword1">Comment</label>
			        					<textarea class="form-control" name="enquiry_comments">{{ $enquiry['enquiry_comments'] }}</textarea>
			        				</div>
				        		</div>
			        		</div>
		        		</div>
		        		<!-- /.card-body -->

		        		<div class="card-footer">
		        			<input type="submit" class="btn btn-primary" value="Add Comment" name="submit">
		        			<input type="button" class="btn btn-danger" value="Cancel" onclick="redirectURL('/admin/enquiries')">
		        		</div>
		        	</form>
		        </div>
		    </div>
		</div>
	</div>
</div>
@endsection